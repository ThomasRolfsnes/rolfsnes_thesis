package main;

import java.io.ObjectInputStream.GetField;

import graphviz.TreeParser;
import core.IEOS;
import core.tree.Tree;

public class Main {
	

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {

		
		//Path to where the graphviz source file should be stored
		String path = "C:\\Users\\ThomasRolf\\Pictures\\out.";
		boolean showComplexity = true;
		boolean showQuery = false;

		IEOS ieos;

		TestData data = new TestData();
		
		ieos = data.updateObjectDiagram();

		
		String example_q = data.getQuery(0);
		String query_nonLin_1 = data.getQuery(1);
		String query_nonLin_2 = data.getQuery(2);
		String query_boardInRange = data.getQuery(3);


		ieos.changeToParsing();
		ieos = data.updateObjectDiagram();
		
		Tree nonLin_1_tree = null;
		Tree nonLin_2_tree = null;
		Tree boardInRange_tree = null;
		Tree example_q_tree = null;
		Tree combined = null;
		try {
			example_q_tree = ieos.parse(example_q);
			nonLin_1_tree = ieos.parse(query_nonLin_1);
			nonLin_2_tree = ieos.parse(query_nonLin_2);
			boardInRange_tree = ieos.parse(query_boardInRange);
			combined = ieos.parse(query_nonLin_1+ " and " + query_nonLin_2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Generating dot format..");
		
		
		
		OCLCC oclCom = new OCLCC();
		Tree tree = boardInRange_tree;
		
		 System.out.println(oclCom.parseTree(tree, true, REFTYPE.LISTID));
		
		
		TreeParser parser = new TreeParser();
		System.out.println(oclCom.complexity(tree.getRootElement(), 0, REFTYPE.CONTAINED, false, null));		
		
		System.out.println("Complexity of expression (contained): " + oclCom.complexity(tree.getRootElement(), 0, REFTYPE.CONTAINED, showQuery, null));
		System.out.println("Complexity of expression (listid): " + oclCom.complexity(tree.getRootElement(), 0, REFTYPE.LISTID, showQuery, null));
		System.out.println("Complexity of expression (associd): " + oclCom.complexity(tree.getRootElement(), 0, REFTYPE.ASSOCID, showQuery, null));
//	
		
		/**
		 * complexity for B.allInstances() -> forAll(b | b.list -> forAll(a | a.x = 0))
		 */
		
		//String bquery = "B.allInstances()->forAll(b | b.a->forAll(a | a.x = 0))";
		//String bquery = "B.allInstances()->forAll(b | b.a->size() > 0)";
		//String bquery = "A.allInstances()->forAll(a | a.x > 0)";
		//String ex = "ElectronicConnection.allInstances()->forAll(e | e.sem.eBoards->size() > 0)";
		
//		Tree btree = null;
//		btree = ieos.parse(ex);
//
//		System.out.println(oclCom.parseTree(btree, false, REFTYPE.CONTAINED));
////		System.out.println(t.parse(btree, path, true, false, REFTYPE.CONTAINED));
//		System.out.println(oclCom.complexity(btree.getRootElement(),0, REFTYPE.CONTAINED, false, null));
	}

	
	
}
