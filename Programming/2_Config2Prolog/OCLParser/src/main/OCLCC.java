package main;

import graphviz.GraphViz;

import java.sql.Ref;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import core.tree.Node;
import core.tree.Tree;

/**
 * OCL Complexity Calculator Use this class to calculate the complexity of OCL
 * expressions. Complexity of an expressions is defined as the number of objects
 * that need to be accessed for the expression to be evaluated
 * 
 * @author ThomasRolf
 * 
 */



public class OCLCC {

	final String X = "x";
	final String P = "P_";
	final String N = "N_";

	/**
	 * NODE INSTANCE SUPERTYPES AND SUBTYPES
	 */
	final String LiteralExp = "LiteralExp";
	final ArrayList<String> LiteralExp_types = new ArrayList<String>(
			Arrays.asList(new String[] { "LiteralExp", "constant" }));

	final String VariableExp = "VariableExp";
	final ArrayList<String> VariableExp_types = new ArrayList<String>(
			Arrays.asList(new String[] { "VariableExp", "variable" }));

	final String AttributeCallExp = "AttributeCallExp";
	final ArrayList<String> AttributeCallExp_types = new ArrayList<String>(
			Arrays.asList(new String[] { "AttributeCallExp", "attribute" }));

	final String IteratorExp = "IteratorExp";
	final ArrayList<String> IteratorExp_types = new ArrayList<String>(
			Arrays.asList(new String[] { "IteratorExp", "iterator" }));

	final String AssociationEndCallExp = "AssociationEndCallExp";
	final ArrayList<String> AssociationEndCallExp_types = new ArrayList<String>(
			Arrays.asList(new String[] { "AssociationEndCallExp", "role" }));

	final String OperationCallExp = "OperationCallExp";
	final ArrayList<String> OperationCallExp_types = new ArrayList<String>(
			Arrays.asList(new String[] { "OperationCallExp", "allInstances","includesAll",
					"and", "<", "<=", ">", ">=", "=", "size" }));

	/**
	 * List of the lists above
	 */
	private final ArrayList<ArrayList<String>> listOfnodeTypes = new ArrayList<ArrayList<String>>(
			Arrays.asList(LiteralExp_types, VariableExp_types,
					AttributeCallExp_types, IteratorExp_types,
					AssociationEndCallExp_types, OperationCallExp_types));

	/**
	 * @param node The node to calculate the complexity for. Start at root to get complexity for full expression.
	 * @param gv 
	 * @param referedVariableLoopExp2 
	 * @return a simple complexity object
	 * @throws Exception 
	 */
	public Complexity complexity(Node node,int parentHash, REFTYPE refType, boolean referedVariableLoopExp, GraphViz gv) throws Exception {

		int hashId = node.hashCode();
		// Complexity of this node
		Complexity cThis = new Complexity("", "");
		// Complexity of the left subtree
		Complexity cL = new Complexity("0", "");
		// Complexity of the right subtree
		Complexity cR = new Complexity("0", "");

		String type = "";
		/**
		 * Check what the supertype of our node is (the node is a subtype, we
		 * only care about the supertype)
		 */
		try {
			type = getInstanceExpType(node);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Node leftChild = null;
		Node rightChild = null;

		try {
			leftChild = node.getChildren().get(0);
		} catch (ArrayIndexOutOfBoundsException e) {}

		try {
			rightChild = node.getChildren().get(1);
		} catch (ArrayIndexOutOfBoundsException e) {}

		/**
		 * FINISHED SETUP, START COMPLEXITY CALCULATION
		 */
		if (leftChild != null && type.equals(IteratorExp)) {
			cL = complexity(leftChild,hashId, refType,true, gv);
			} else if(leftChild != null && referedVariableLoopExp){				
				cL = complexity(leftChild,hashId, refType,true, gv);
			} else if(leftChild != null ){				
				cL = complexity(leftChild,hashId, refType,false, gv);
			}
		if (rightChild != null) {cR = complexity(rightChild, hashId, refType,false, gv);}
		
		switch (type) {
		case LiteralExp:
			cThis.setAux("0");
			cThis.setCompl("0");
			break;
		case VariableExp:
			cThis.setAux("1");
			if (referedVariableLoopExp) {
				cThis.setCompl("1");
			} else {
				cThis.setCompl("0");
			}
			break;
		case AttributeCallExp:
			cThis.setAux(cL.getAux());
			cThis.setCompl(cL + "");
			break;
		case IteratorExp:
			cThis.setAux(cL.getAux());
			cThis.setCompl(cL + "+" + cL.getAux() + X + cR);
			break;
		case AssociationEndCallExp:
			switch (refType) {
			case CONTAINED:
				if (isMultiplicityOne(node)) {
					cThis.setAux(cL.getAux());
					cThis.setCompl(cL + "+" + cL.getAux());
				} else {
					cThis.setAux(cL.getAux()+ X + N + getAssosiationEndName(node));
					cThis.setCompl(cL + "+" +  cL.getAux() + X + N
							+ getAssosiationEndName(node));
				}
				break;
			case LISTID:
				if (isMultiplicityOne(node)) {
					cThis.setAux(cL.getAux());
					cThis.setCompl(cL + "+" + cL.getAux() + X + P + getType(node));
				} else {
					throw new Exception("List-id: no implementation for higher multiplicities yet");
				}
				break;
			case ASSOCID:
				if (isMultiplicityOne(node)) {
					cThis.setAux(cL.getAux());
					cThis.setCompl(cL + "+" + cL.getAux() + X + "log(" + P + getType(node) + ")");
				} else {
					throw new Exception("Assoc-id: no implementation for higher multiplicities yet");
					}
				break;
			}
			break;
		case OperationCallExp:
			if (node.getNameOperation().equals("allInstances")) {
				cThis.setAux(P + getType(node));
				cThis.setCompl(P + getType(node));
			} else if (node.getNameOperation().equals("includesAll")) {
				cThis.setAux(cL + X +"log(" + cR +")");
				cThis.setCompl(cL + X +"log(" + cR +")" );

			} else { // operations like '+', '-', '*', '<', '>=','and' etc
				cThis.setAux("0");
				cThis.setCompl(cL + "+" + cR);
			}
			break;
		}
		cThis.setAux(cleanUp(cThis.getAux()));
		cThis.setCompl(cleanUp(cThis.getCompl()));
		
		if(gv != null){
			addLabel(node, cThis, parentHash, refType, gv);
		}
		return cThis;
	}
	
	

	/**
	 * Removes the following substrings:
	 * +(0)
	 * (0)+
	 * 0+
	 * +0
	 * @param s the complexity function to clean
	 * @return
	 */
	public String cleanUp(String s) {
		String removeZeroes = s.replaceAll("(\\+\\(0\\))|(\\(0\\)\\+)|(0\\+)|(\\+0)|(\\+\\(\\(.*\\)x0\\))", "");
		
		String fixX = removeZeroes.replaceAll("x", " x ");
		String noWhitespace  = fixX.replaceAll("\\s{2,}", ""); // remove unnecessary whitespace
		if(noWhitespace.contains(" ") && !noWhitespace.startsWith("(") && !noWhitespace.endsWith(")")){
			return "(" + noWhitespace + ")";			
		} else {
			return noWhitespace;
		}
		
	}

	private String printInfo(Node node) {
		String s = "";
		for (Object o : node.getInfoOperation()) {
			s += o + " ";
		}
		return s;
	}

	private boolean isIteratorVariable(Node node) {
		boolean itVar = false;
		if (node.getNameOperation().equals("variable")) {
			itVar = node.getInfoOperation()[1].equals(-1);
		}
		return itVar;
	}

	private boolean isMultiplicityOne(Node node) {
		String s = (String) node.getInfoOperation()[1];
		return !s.matches("Set\\(\\w+\\)");
	}

	private String getAssosiationEndName(Node node) {
		String s = ((String) node.getInfoOperation()[0]);
		return s;
	}

	private String getType(Node node) {
		String s = ((String) node.getInfoOperation()[0]);
		
		if(s.matches("\\w+\\(\\w+\\)")){
			s = s.substring(s.indexOf("(")+1, s.indexOf(")"));
		}
		return s.toLowerCase();
	}

	private String getInstanceExpType(Node node) throws Exception {
		String name = node.getNameOperation();
		String type = "";

		for (ArrayList<String> nodeTypes : getListOfNodeTypes()) {
			if (nodeTypes.contains(name)) {
				type = nodeTypes.get(0);
				return type;
			}
		}

		throw new Exception("Could not find expression type (" + name + ")");
	}

	public ArrayList<ArrayList<String>> getListOfNodeTypes() {
		return listOfnodeTypes;
	}
	
	public String parseTree(Tree tree, boolean showQuery, REFTYPE refType) throws Exception{
		GraphViz gv = new GraphViz();
		gv.addln(gv.start_graph());

		Node root = tree.getRootElement();

		if(showQuery){
			gv.addln("query [label=\"AST for: " + root.print()
					+ "\",shape=\"plaintext\"];");
			gv.addln("query -> " + root.hashCode() + "[color=\"white\"]");			
		}
		complexity(root, 0, refType, false,gv);

		gv.addln(gv.end_graph());
		
		return gv.getDotSource();
	}
	
	
	private void addLabel(Node node,Complexity c,int parentHash,REFTYPE refType,GraphViz gv) {
		String s = "";
			String aux = c.getAux();
			String compl = c.getCompl();
			s = node.hashCode() + " [shape=box,label=\"" + node.getNameOperation()
					+ printInfo(node.getInfoOperation()) + "\nCompl: " + compl + "\\lAux: "+ aux +"\\l\"];";
		

		gv.addln(s);
		if(parentHash != 0){
			gv.addln(parentHash + " -> " + node.hashCode() + ";");			
		}

	}
	
	private String printInfo(Object[] info) {
		String result = "";
		if (info != null) {
			result = "(";
			for (int i = 0; i < info.length; i++) {
				if (i == info.length - 1) {
					if (info[i].getClass() == Node.class) {
						result = result + ((Node) info[i]).getNameOperation();// toString();
					} else {
						result = result + info[i].toString();
					}
				} else {
					if (info[i].getClass() == Node.class) {
						result = result + ((Node) info[i]).getNameOperation()
								+ ",";

					} else {
						result = result + info[i].toString() + ",";
					}

				}
			}
			result = result + ")";
		}
		return result;
	}
}
