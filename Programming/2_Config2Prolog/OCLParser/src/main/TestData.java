package main;

import java.util.Collection;

import core.IEOS;
import core.tree.Tree;


public class TestData {
	
	private IEOS ieos;
	
	private String EC = "ElectronicConnection";
	private String ebIndex = "ebIndex";
	private String pinIndex = "pinIndex";
	//SEM
	private String SEM = "SEM";
	private String eBoards = "eBoards";
	
	//Enumeration
	private String ElecBoard = "ElecBoard";
	
	
	
	
	
	
//	Inv1: context ElectronicConnection inv PinRange
//	pinIndex >= 0 and sem.eBoards->asSequence()->at(ebIndex+1).numOfPins > pinIndex
//
//	Inv2: context ElectronicConnection inv BoardIndRange 
//	ebIndex >= 0 and ebIndex < sem.eBoards->size()
	
	private String[] queries = new String[]{
			"ElectronicConnection.allInstances()->forAll(e|e.sem->size()>0)", //EOS example query
			"ElectronicConnection.allInstances()->select(c|c.pinIndex = 0)->collect(c|c.sem)->includesAll(SEM.allInstances())", //quadratic 1
			"ElectronicConnection.allInstances()->collect(c|c.sem.eBoards)->size() >= SEM.allInstances()->collect(c|c.eBoards)->size()", //quadratic 2
			EC+".allInstances()->forAll(ec | ec.ebIndex > 0 and ec.ebIndex <= ec.sem.eBoards->size())", //board in range
			EC+".allInstances()->forAll(ec | ec.pinIndex >= 0 and ec.sem.eBoards->asSequence()->at(ec.ebIndex+1).numOfPins > pinIndex)", //pin range
			EC+".allInstances()->forAll(ec | ec.ebIndex >= 0 and ec.ebIndex < ec.sem.eBoards->size()) and "+EC+".allInstances()->forAll(ec | ec.pinIndex >= 0 and ec.sem.eBoards->asSequence()->at(ec.ebIndex+1).numOfPins > pinIndex)",//combined	
		};
	
	
	public TestData(){
		ieos = new IEOS();
	}
	
	public IEOS updateObjectDiagram() throws Exception {
		
		ieos.createClassDiagram();

		ieos.insertEnumeration("ElecBoard", new String[]{"8_PIN","16_PIN","32_PIN","64_PIN"});

		//Class Electronic Connection
		ieos.insertClass("ElectronicConnection");
		ieos.insertAttribute("ElectronicConnection", "ebIndex", "Integer");
		ieos.insertAttribute("ElectronicConnection", "pinIndex", "Integer");
		//Class SEM
		ieos.insertClass("SEM");
		ieos.insertAttribute("SEM", "eBoards", "ElecBoard");
		
		//Associations
		ieos.insertAssociation("ElectronicConnection", "ec","1..*","0..1", "sem", "SEM");
		
		/**
		 * A case
		 */
		
//		ieos.insertClass("A");
//		ieos.insertAttribute("A", "x", "Integer");
//		ieos.insertClass("B");
//		
//		ieos.insertAssociation("B", "b", "1", "1..*", "a", "A");
		
		
		
		
		ieos.closeClassDiagram();
		
	
		/**
		 * Object Diagram
		 */
		
		ieos.createObjectDiagram();
//
//		ieos.insertObject(EC, "ElecCon");
//		ieos.insertValue(EC, pinIndex, "ElecCon", "1");
//		ieos.insertValue(EC, ebIndex, "ElecCon", "1");
//		
//		ieos.insertObject(SEM, "sem");
//		
//		//ieos.insertObject(ElecBoard, "board1");
//		ieos.insertValue(SEM, "eBoards", "sem", "a");
//		
//		//ieos.insertLink(SEM, "sem", "sem", "eBoards", "board1", ElecBoard);
//	//	ieos.insertValue(SEM, eBoards, "sem", "8");
//		
//		ieos.insertLink(EC, "ElecCon", "ec", "sem", "sem", SEM);
		
		//ieos.insertObject(ElecBoard, "int0");
		//ieos.insertObject(ElecBoard, "int1");
		
		//ieos.insertObject("Int", "int0");
		//ieos.insertValue(ElecBoard, "numOfPins", "int0", "8");
		//ieos.insertValue(ElecBoard, "numOfPins", "int1", "16");
//		
//		ieos.insertObject("Int", "int1");
//		ieos.insertValue("Int", "numOfPins", "int1", "16");
//		
//		ieos.insertObject("Int", "int2");
//		ieos.insertValue("Int", "numOfPins", "int2", "32");
//		
//		ieos.insertObject("Int", "int3");
//		ieos.insertValue("Int", "numOfPins", "int3", "64");
		
		//ieos.insertLink(ElecBoard, "eBoards", "eBoard", "literals", "int0", "Int");
//		ieos.insertLink(ElecBoard, "eBoards", "eBoard", "int", "int1", "Int");
//		ieos.insertLink(ElecBoard, "eBoards", "eBoard", "int", "int2", "Int");
//		ieos.insertLink(ElecBoard, "eBoards", "eBoard", "int", "int3", "Int");
		
		//ieos.insertLink(SEM, "sem", "sem", "eBoards", "int0", ElecBoard);
		//ieos.insertLink(SEM, "sem", "sem", "eBoards", "int1", ElecBoard);
		
//
//		ieos.insertObject("TestClass", "TestClass");
//		ieos.insertValue("TestClass", "name", "TestClass", "name");
//		ieos.insertValue("TestClass", "salary", "TestClass", "1000");
//
//		
//		ieos.insertLink(SEM, "sem", "sem", "testClass", "TestClass", "TestClass");
//		ieos.insertLink(EC, "ElecCon", "ec","sem", "sem",SEM);
		
		
		
		try {
			ieos.closeObjectDiagram();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Unable to close ObjectDiagram");
		}
		
		return ieos;
		
	
		
	}
	
	public IEOS getIEOS(){
		return ieos;
	}
	
	public String getQuery(int chosenQuery){
		if(chosenQuery > queries.length-1){
			System.out.println("Element with that id not present, giving you the first element instead.");
			return queries[0];
		} else {
			return queries[chosenQuery];
		}
	}
	
	

}
