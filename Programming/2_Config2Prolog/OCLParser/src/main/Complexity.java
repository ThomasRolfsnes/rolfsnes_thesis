package main;

public class Complexity {

	private String compl;
	private String aux;
	
	/***
	 * 
	 * @param compl
	 * @param aux
	 */
	public Complexity(String compl,String aux){
		this.setCompl(compl);
		this.setAux(aux);
	}

	/**
	 * @return the compl
	 */
	public String getCompl() {
		return compl;
	}

	/**
	 * @param compl the compl to set
	 */
	public void setCompl(String compl) {
		this.compl = compl;
	}

	/**
	 * @return the aux
	 */
	public String getAux() {
		return aux;
	}

	/**
	 * @param aux the aux to set
	 */
	public void setAux(String aux) {
		this.aux = aux;
	}


	public String toString(){
		return getCompl();
	}
}
