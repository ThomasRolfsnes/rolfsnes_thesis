package graphviz;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOError;
import java.io.PrintWriter;

import main.Complexity;
import main.OCLCC;
import main.REFTYPE;

import core.tree.Node;
import core.tree.Tree;

public class TreeParser {

	private GraphViz gv;
	private OCLCC c;

	public TreeParser() {
		gv = new GraphViz();
		c = new OCLCC();
	}

	/**
	 * 
	 * @param tree
	 *            The IEOS tree object
	 * @param outputPath
	 *            Give the system path where you want to generate the AST image.
	 *            Set to null if no image should be generated.
	 * @param refType 
	 * @return The tree in dot format
	 * @throws Exception 
	 */
	public String parse(Tree tree, String outputPath,boolean showComplexity, boolean showQuery, REFTYPE refType) throws Exception {

		gv.addln(gv.start_graph());

		Node root = tree.getRootElement();

		if(showQuery){
			gv.addln("query [label=\"AST for: " + root.print()
					+ "\",shape=\"plaintext\"];");
			gv.addln("query -> " + root.hashCode() + "[color=\"white\"]");			
		}
		parseSubTree(root, showComplexity, refType);

		gv.addln(gv.end_graph());

		String type = "gif";
		// String type = "dot";
		// String type = "fig"; // open with xfig
		// String type = "pdf";
		// String type = "ps";
		// String type = "svg"; // open with inkscape
		// String type = "png";
		// String type = "plain";
		// File out = new File("/tmp/out." + type); // Linux
		if (outputPath != null) {
			try {
				//Currently just writes the graphviz source to the specified file path
				String location = outputPath + "gv";
				
				//The below is non working code for generating an img file directly
				// File out = new File(location); // Windows
				// gv.writeGraphToFile( gv.getGraph( gv.getDotSource(), type ),
				// out );
//				byte[] img = gv.getGraph(gv.getDotSource(), type);
//				if (img == null) {
//					System.out.println("Unable to generate byte[] image");
//				} else {
//					gv.writeGraphToFile(img, location);
//				}
				PrintWriter out = new PrintWriter(location);
				out.write(gv.getDotSource());
				out.close();
				
			} catch (IOError e) {
				System.out.println(e);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return gv.getDotSource();

	}

	private void parseSubTree(Node node, boolean showComplexity,REFTYPE reftype) throws Exception {
		gv.addln(addLabel(node, showComplexity, reftype));
		for (Node e : node.getChildren()) {
			gv.addln(addLabel(e, showComplexity,reftype));
			gv.addln(node.hashCode() + " -> " + e.hashCode() + ";");

			parseSubTree(e, showComplexity, reftype);

		}
	}

	private String addLabel(Node node,boolean showComplexity,REFTYPE refType) throws Exception {
		String s = "";
		if(showComplexity){
			Complexity nodeCompl = c.complexity(node,0, refType,false, gv);
			String aux = nodeCompl.getAux();
			String compl = nodeCompl.getCompl();
			s = node.hashCode() + " [shape=box,label=\"" + node.getNameOperation()
					+ printInfo(node.getInfoOperation()) + "\nCompl: " + compl + "\\lAux: "+ aux +"\\l\"];";
		} else {
			s = node.hashCode() + " [shape=box,label=\"" + node.getNameOperation()
					+ printInfo(node.getInfoOperation()) + "\"];";

		}

		return s;
	}

	private String printInfo(Object[] info) {
		String result = "";
		if (info != null) {
			result = "(";
			for (int i = 0; i < info.length; i++) {
				if (i == info.length - 1) {
					if (info[i].getClass() == Node.class) {
						result = result + ((Node) info[i]).getNameOperation();// toString();
					} else {
						result = result + info[i].toString();
					}
				} else {
					if (info[i].getClass() == Node.class) {
						result = result + ((Node) info[i]).getNameOperation()
								+ ",";

					} else {
						result = result + info[i].toString() + ",";
					}

				}
			}
			result = result + ")";
		}
		return result;
	}

}
