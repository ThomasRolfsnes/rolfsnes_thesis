/*
 * Copyright (c) 2004 SICS AB
 */

package se.sics.jasper;

// [PD] 3.12.3+ Does not have to be public
//public class Version
class Version
{
    static String SICStusVersion = "4.2.3";

    static String getSICStusVersion()
    {
	return SICStusVersion;
    }
}
