package rolfsnes.cp.prologexperiments.prologquerygenerator;

import java.util.HashMap;
import java.util.Map;


public class ExperimentResult {
	
	public static final String var_refType = "ReferenceType";
	public static final String var_modelRepresentation = "ModelRepresentation";
	public static final String var_constrainType= "ConstraintType";
	public static final String var_configuration= "Configuration";
	public static final String var_pred_organization = "PredicateOrganization";
	
	public static final String var_independent_NumberOfClasses = "NumberOfObjects";
	public static final String var_independent_NumberOfConfiguredAttributes = "NumberOfConfiguredAttributes";
	public static final String var_independent_NumberOfUnConfiguredAttributes = "NumberOfUnConfiguredAttributes";
	public static final String var_independent_ComplexityOfOCL = "ComplexityOfOCL";
	public static final String var_dependent_objectModelToPrologQueryTime = "ObjectModelToPrologQueryTime";
	public static final String var_dependent_prologEvaluationTime = "PrologEvaluationTime";
	
	public static final String var_dependent_resumptions = "resumptions";
	public static final String var_dependent_entailments = "entailments";
	public static final String var_dependent_prunings = "prunings";
	public static final String var_dependent_backtracks = "backtracks";
	public static final String var_dependent_constraints = "constraints";

	public static final String[] csvHeader = new String[]{ 
		var_refType,
		var_modelRepresentation,
		var_constrainType,
		var_configuration,
		var_pred_organization,
														   var_independent_ComplexityOfOCL,
														   var_independent_NumberOfClasses,
														   var_dependent_objectModelToPrologQueryTime,
														   var_dependent_prologEvaluationTime,
														   var_dependent_resumptions,
														   var_dependent_entailments,
														   var_dependent_prunings,
														   var_dependent_backtracks,
														   var_dependent_constraints,
														   var_independent_NumberOfUnConfiguredAttributes,
														   var_independent_NumberOfConfiguredAttributes};
	
	private StringBuilder solutionSet = new StringBuilder();
	/**
	 * Independent variables
	 */
	
	private String refType;
	private String modelRepresentation;
	private String constrainType;
	private String configuration;
	private String predOrganization;
	

	private final Map<String,Integer> independent_NumberOfClasses = new HashMap<String,Integer>();
	private final Map<String,Integer> independent_NumberOfConfiguredAttributes = new HashMap<String,Integer>();
	private final Map<String,Integer> independent_NumberOfUnConfiguredAttributes = new HashMap<String,Integer>();
	private long independent_ComplexityOfOCL;
	
	
	

	/**
	 * Dependent variables
	 */
	private long dependent_objectModelToPrologQueryTime;
	private String dependent_prologEvaluationTime;
	private String dependent_resumptions;
	private String dependent_entailments;
	private String dependent_prunings;
	private String dependent_backtracks;
	private String dependent_constraints;
	
	
	public String toString(){
		StringBuilder s = new StringBuilder();
		s.append("Number of Classes:" + independent_NumberOfClasses +"\n\n");
		s.append("Number of Configured Attributes:" + independent_NumberOfConfiguredAttributes +"\n\n");
		s.append("Number of Unconfigured Attributes:" + independent_NumberOfUnConfiguredAttributes + "\n\n");
		s.append("Complexity of OCL constraints:" + independent_ComplexityOfOCL + "\n\n\n\n");
		
		s.append("Object model to prolog query: " + dependent_objectModelToPrologQueryTime +" ms\n\n");
		s.append("Prolog evaluation: " + dependent_prologEvaluationTime +" ms\n\n");
		
		s.append("Resumptions: " + dependent_resumptions + "\n\n");
		s.append("Entailments: " + dependent_entailments+ "\n\n");
		s.append("Prunings: " + dependent_prunings+ "\n\n");
		s.append("Backtracks: " + dependent_backtracks+ "\n\n");
		s.append("Constraints: " + dependent_constraints+ "\n\n");
		
		return s.toString();
		
	}
	
	/**
	 * Add a solution to the solution set. Uses the fd_dom(X,Y) predicate to get
	 * the domain under clpfd evaluation
	 * 
	 * @param s
	 *            the variable/attribute to add. Will only be added to the
	 *            solution set if it's currently unconfigured (that is,it is in
	 *            prolog variable form)
	 */
	public void addSolution(String s) {
			if (this.getSolutionSet().length() == 0) {
				this.getSolutionSet().append("fd_dom(" + s + "," + s + "_dom)");
			} else {
				// from second addition to the solution set, add leading comma
				this.getSolutionSet()
						.append(",fd_dom(" + s + "," + s + "_dom)");
			}
		
	}
	
	/**
	 * Build a list of the fields declared in this class, suitable for a csv file header
	 * @return the list of fields of this class
	 */
	public String[] getCSVHeader(){
		return csvHeader;
	}
	
	
	/**
	 * @return the independent_NumberOfClasses
	 */
	public Map<String,Integer> getIndependent_NumberOfClasses() {
		return independent_NumberOfClasses;
	}
	/**
	 * @return the independent_NumberOfConfiguredAttributes
	 */
	public Map<String,Integer> getIndependent_NumberOfConfiguredAttributes() {
		return independent_NumberOfConfiguredAttributes;
	}
	/**
	 * @return the independent_NumberOfUnConfiguredAttributes
	 */
	public Map<String,Integer> getIndependent_NumberOfUnConfiguredAttributes() {
		return independent_NumberOfUnConfiguredAttributes;
	}
	public long getIndependent_ComplexityOfOCL() {
		return independent_ComplexityOfOCL;
	}
	public void setIndependent_ComplexityOfOCL(long l) {
		this.independent_ComplexityOfOCL = l;
	}
	/**
	 * @return the dependent_objectModelToPrologQueryTime
	 */
	public long getDependent_objectModelToPrologQueryTime() {
		return dependent_objectModelToPrologQueryTime;
	}
	/**
	 * @param dependent_objectModelToPrologQueryTime the dependent_objectModelToPrologQueryTime to set
	 */
	public void setDependent_objectModelToPrologQueryTime(
			long dependent_objectModelToPrologQueryTime) {
		this.dependent_objectModelToPrologQueryTime = dependent_objectModelToPrologQueryTime;
	}
	/**
	 * @return the dependent_prologEvaluationTime
	 */
	public String getDependent_prologEvaluationTime() {
		return dependent_prologEvaluationTime;
	}
	/**
	 * @param string the dependent_prologEvaluationTime to set
	 */
	public void setDependent_prologEvaluationTime(
			String string) {
		this.dependent_prologEvaluationTime = string;
	}
		

	public int getTotalNumberOfClasses(){
		int result = 0;
		for (Integer i : getIndependent_NumberOfClasses().values()) {
			result += i;
		}
		return result;
	}
	
	public int getTotalNumberOfConfiguredAttributes(){
		int result = 0;
		for (Integer i : getIndependent_NumberOfConfiguredAttributes().values()) {
			result += i;
		}
		return result;
	}

	public int getTotalNumberOfUnconfiguredAttributes(){
		int result = 0;
		for (Integer i : getIndependent_NumberOfUnConfiguredAttributes().values()) {
			result += i;
		}
		return result;
	}

	/**
	 * @return the solutionSet
	 */
	public StringBuilder getSolutionSet() {
		return solutionSet;
	}

	/**
	 * @param solutionSet the solutionSet to set
	 */
	public void setSolutionSet(StringBuilder solutionSet) {
		this.solutionSet = solutionSet;
	}

	/**
	 * @return the dependent_resumptions
	 */
	public String getDependent_resumptions() {
		return dependent_resumptions;
	}

	/**
	 * @param string the dependent_resumptions to set
	 */
	public void setDependent_resumptions(String string) {
		this.dependent_resumptions = string;
	}

	/**
	 * @return the dependent_prunings
	 */
	public String getDependent_prunings() {
		return dependent_prunings;
	}

	/**
	 * @param string the dependent_prunings to set
	 */
	public void setDependent_prunings(String string) {
		this.dependent_prunings = string;
	}

	/**
	 * @return the dependent_entailments
	 */
	public String getDependent_entailments() {
		return dependent_entailments;
	}

	/**
	 * @param string the dependent_entailments to set
	 */
	public void setDependent_entailments(String string) {
		this.dependent_entailments = string;
	}

	/**
	 * @return the dependent_backtracks
	 */
	public String getDependent_backtracks() {
		return dependent_backtracks;
	}

	/**
	 * @param string the dependent_backtracks to set
	 */
	public void setDependent_backtracks(String string) {
		this.dependent_backtracks = string;
	}

	/**
	 * @return the dependent_constraints
	 */
	public String getDependent_constraints() {
		return dependent_constraints;
	}

	/**
	 * @param string the dependent_constraints to set
	 */
	public void setDependent_constraints(String string) {
		this.dependent_constraints = string;
	}


	/**
	 * @return the refType
	 */
	public String getRefType() {
		return refType;
	}

	/**
	 * @param refType the refType to set
	 */
	public void setRefType(String refType) {
		this.refType = refType;
	}

	/**
	 * @return the modelRepresentation
	 */
	public String getModelRepresentation() {
		return modelRepresentation;
	}

	/**
	 * @param modelRepresentation the modelRepresentation to set
	 */
	public void setModelRepresentation(String modelRepresentation) {
		this.modelRepresentation = modelRepresentation;
	}

	/**
	 * @return the constrainType
	 */
	public String getConstrainType() {
		return constrainType;
	}

	/**
	 * @param constrainType the constrainType to set
	 */
	public void setConstrainType(String constrainType) {
		this.constrainType = constrainType;
	}

	/**
	 * @return the configuration
	 */
	public String getConfiguration() {
		return configuration;
	}

	/**
	 * @param configuration the configuration to set
	 */
	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	/**
	 * @return the predOrganization
	 */
	public String getPredOrganization() {
		return predOrganization;
	}

	/**
	 * @param predOrganization the predOrganization to set
	 */
	public void setPredOrganization(String predOrganization) {
		this.predOrganization = predOrganization;
	}
	
	

	
}
