package rolfsnes.cp.prologexperiments.experiments;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import rolfsnes.cp.prologexperiments.prologquerygenerator.ExperimentResult;
import rolfsnes.cp.prologexperiments.prologquerygenerator.ExperimentSetup;
import rolfsnes.cp.prologexperiments.prologquerygenerator.ObjectModel;

public class ExperimentCoordinater {

	static ObjectModel om;
	static String prologQuery; 
	static ArrayList<ExperimentSetup> setups = new ArrayList<>();
	static ArrayList<ExperimentResult> results = new ArrayList<>();
	
	
	
	
	public static ArrayList<ExperimentSetup> getSetups() {
		return setups;
	}

	public static void setSetups(Collection<? extends ExperimentSetup> collection) {
		ExperimentCoordinater.setups.addAll(collection);
		collection.clear();
	}

	public static ArrayList<ExperimentResult> getResults() {
		return results;
	}

	public static void setResults(ArrayList<ExperimentResult> results) {
		ExperimentCoordinater.results = results;
	}

	

	public static String getPrologQuery(){
		return prologQuery;
	}
	
	public static void setPrologQuery(String prologQuery){
		ExperimentCoordinater.prologQuery = prologQuery;
		prologQuery = "";
	}
	
	public static ObjectModel getOm() {
		return om;
	}

	public static void setOm(ObjectModel om) {
		ExperimentCoordinater.om = om;
	}

}
