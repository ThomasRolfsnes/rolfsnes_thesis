package rolfsnes.cp.prologexperiments.prologquerygenerator;

import rolfsnes.cp.prologexperiments.exceptions.OutOfBoundsException;
import rolfsnes.cp.prologexperiments.exceptions.PrologSyntaxException;

public class AttributeConfiguration {

	private String name;
	private String varName;
	private Domain domain;
	private double percentageConfigurated;
	
	/**
	 * @param name the name of the attribute
	 * @param varName how this attribute should look like as a variable (unconfigured)
	 * @param domain set the domain (possible value range) of this attribute
	 * @param percentageConfigurated what percentage of instantiations of this attribute should be configurated?
	 */
	public AttributeConfiguration(String name, String varName, Domain domain,
			double percentageConfigurated) {
		this.setName(name);
		this.setVarName(varName);
		this.setDomain(domain);
		this.setPercentageConfigurated(percentageConfigurated);
	}

	/**
	 * Generates a PrologAttribute based on the the current configuration
	 * @return the PrologAttribute
	 * @throws PrologSyntaxException 
	 * @throws OutOfBoundsException 
	 */
	public PrologAttribute generatePrologAttribute(PrologClass parentClass, ExperimentSetup experimentSetup) throws PrologSyntaxException, OutOfBoundsException{
		
		return new PrologAttribute(getName(),(getDomain().isListInstance()) ? getDomain().getList() : getDomain().getRandomInstantiation()+"",
				getVarName(),				   
				parentClass, experimentSetup);
	}
	
	/**
	 * @return the percentageConfigurated
	 */
	public double getPercentageConfigurated() {
		return percentageConfigurated;
	}

	/**
	 * @param percentageConfigurated the percentageConfigurated to set
	 */
	public void setPercentageConfigurated(double percentageConfigurated) {
		this.percentageConfigurated = percentageConfigurated;
	}

	/**
	 * @return the domain
	 */
	public Domain getDomain() {
		return domain;
	}

	/**
	 * @param domain the domain to set
	 */
	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	/**
	 * @return the varName
	 */
	public String getVarName() {
		return varName;
	}

	/**
	 * @param varName the varName to set
	 */
	public void setVarName(String varName) {
		this.varName = varName;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}



	



	
	
}
