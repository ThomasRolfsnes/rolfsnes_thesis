package rolfsnes.cp.prologexperiments.exceptions;

public class PrologEvaluationException extends Exception {

	public PrologEvaluationException() {
	}

	public PrologEvaluationException(String message) {
		super(message);
	}

	public PrologEvaluationException(Throwable cause) {
		super(cause);
	}

	public PrologEvaluationException (String message, Throwable cause) {
		super(message, cause);
	}
}
