package rolfsnes.cp.prologexperiments.exceptions;

public class PrologFileRestoreException extends Exception{
	
	public PrologFileRestoreException() {
	}

	public PrologFileRestoreException(String message) {
		super(message);
	}

	public PrologFileRestoreException(Throwable cause) {
		super(cause);
	}

	public PrologFileRestoreException (String message, Throwable cause) {
		super(message, cause);
	}

}
