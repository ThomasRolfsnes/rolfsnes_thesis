package rolfsnes.cp.prologexperiments.prologquerygenerator;

import java.util.concurrent.atomic.AtomicInteger;


public abstract class PrologObject {
	
	private static AtomicInteger uniqueId = new AtomicInteger();
	private String ID;
	
	private final ExperimentSetup experimentSetup;
	
	
	/**
	 * Create new object and set a new unique id (+1 from previously generated id)
	 */
	public PrologObject(ExperimentSetup experimentSetup){
		
		this.ID = Integer.toString(uniqueId.getAndIncrement());
		this.experimentSetup = experimentSetup;

	}
	
	public PrologObject(String id,ExperimentSetup experimentSetup){
		setID(id);
		this.experimentSetup = experimentSetup;
	}
	
	
	abstract public String getName();
	
	public String getShortName(){
		return getName().substring(0, 3);
	}
	
	
	/**
	 * 
	 * @param withIDIdentifier concatenates "id-" to the objects id
	 * @return
	 */
	public String getID(boolean withIDIdentifier,boolean withClassIdentifier){
		String id;
		if(withIDIdentifier && withClassIdentifier){
			id = "id-" + getName()+"_" + this.ID;
		} else if(withIDIdentifier) {
			id = "id-" + this.ID;
		} else if(withClassIdentifier){
			id = getName()+"_" + this.ID;
		} else {
			id = this.ID;
		}
		return id;
	}
	
	public void setID(String id){
		this.ID = id;
	}
	
	protected abstract void increaseCount();
	
	
	/**
	 * Should be implemented to return the prolog representation of an object.
	 * All variables/attributes that you want solution domains for in
	 * the prolog evaluation, must use the input pqg object to add solutions like this:
	 * pqg.addSolution(getX()), where getX() returns the string representation of a variable/attribute
	 * @param pqg
	 * @return the wanted prolog representation of an object
	 */
	public abstract StringBuilder toProlog();
	

	/**
	 * @return the experimentSetup
	 */
	public ExperimentSetup getExperimentSetup() {
		return experimentSetup;
	}

	public String toString(){
		return "[class="+getName()+",id="+getID(false,true)+"]";
	}


	public abstract StringBuilder toProlog(boolean addTypeIdentifier);

	public String generateID(){
		return PrologObject.uniqueId.getAndIncrement()+"";
	}

	

}
