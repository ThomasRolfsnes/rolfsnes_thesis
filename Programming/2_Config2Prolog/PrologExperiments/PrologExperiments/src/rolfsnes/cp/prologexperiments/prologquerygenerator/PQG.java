package rolfsnes.cp.prologexperiments.prologquerygenerator;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import javax.annotation.processing.FilerException;

import rolfsnes.cp.prologexperiments.exceptions.OutOfBoundsException;
import rolfsnes.cp.prologexperiments.exceptions.PrologEvaluationException;
import rolfsnes.cp.prologexperiments.exceptions.PrologFileRestoreException;
import rolfsnes.cp.prologexperiments.exceptions.PrologSyntaxException;
import rolfsnes.cp.prologexperiments.exceptions.UniquenessException;
import rolfsnes.cp.prologexperiments.experiments.ExperimentCoordinater;
import se.sics.jasper.SPException;
import sun.security.x509.AVA;
import au.com.bytecode.opencsv.CSVWriter;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;

public abstract class PQG {

	private final PrologInterface prologInterface;
	private ArrayList<ExperimentSetup> setups = new ArrayList<ExperimentSetup>();
	private String prologFileName;

	public PQG(PrologInterface prologInterface) throws SPException {
		this.prologInterface = prologInterface;
	}

	/**
	 * @param pl_path
	 *            path to the prolog file you want to interface with
	 * @throws SPException
	 */
	public PQG(String pl_path, ArrayList<ExperimentSetup> setups,PrologInterface prologInterface)
			throws SPException {
		this.prologInterface = prologInterface;
		this.setups = setups;
		this.prologInterface.load(pl_path);
		setPrologFileName(pl_path);
	}

	/**
	 * Should be implemented so that it returns an instance of ObjectModel,
	 * which should have all classes stored and configured/unconfigured
	 * according to the provided ExperimentSetup
	 * 
	 * @param e
	 * @return
	 * @throws PrologSyntaxException
	 */
	public abstract ObjectModel buildObjectModel(ExperimentSetup e)
			throws PrologSyntaxException, UniquenessException,
			OutOfBoundsException;

	/**
	 * Main method for generating the prolog query. Here everything that is
	 * outside your objects must be specified. (Before and after each class
	 * generation). Use the toProlog() method on classes. The string should
	 * start from right where getQueryHeaders() stops (right after the names of
	 * your queries)
	 * 
	 * @param om
	 *            the ObjectModel that has the classes we want to generate from
	 * @return the prolog query
	 */
	public abstract StringBuilder getPrologQuery(ObjectModel om,
			ExperimentSetup setup, boolean generateSolutionSet);

	public StringBuilder generateSolutionSet(ExperimentSetup setup,
			StringBuilder query) {

		int index = setup.getNumOfClassIndex();
		if (setup.getExperimentResult().get(index).getSolutionSet().length() != 0) {
			query.append(",");
			query.append(setup.getExperimentResult().get(index)
					.getSolutionSet());
			setup.getExperimentResult().get(index).getSolutionSet()
					.setLength(0);
		}

		return query;

	}

	/**
	 * Runs the current configuration. Dependent on the implementation
	 * pqg.buildObjectModel/getPrologQuery, and ExperimentSetups initialized in
	 * this pqg object.
	 * 
	 * @param randomizeSetups
	 *            if the list of setups should be randomized before running
	 * @return A list of results, one result per ExperimentSetup.
	 * @throws NoSuchMethodException
	 * @throws InterruptedException
	 * @throws Exception
	 */
	public void Run(int averageOverXRuns,boolean randomizeSetups,
			boolean generateSolutionSet) throws NoSuchMethodException,
			InterruptedException, Exception {
		if (ExperimentCoordinater.getSetups().size() == 0) {
			throw new NullPointerException(
					"No ExperimentSetups has been added to the PQG");
		}
		if (randomizeSetups) {
			Collections.shuffle(ExperimentCoordinater.getSetups());
		}

		int i = 0;
		for (ExperimentSetup s : ExperimentCoordinater.getSetups()) {

			do {
				// reloads prolog file to ensure similar context for all runs
				getPrologInterface().load(getPrologFileName());

				ExperimentResult r = new ExperimentResult();
				s.getExperimentResult().add(s.getNumOfClassIndex(), r);

				ExperimentCoordinater.getResults().add(runOneExperimentConfiguration(s,averageOverXRuns,
						generateSolutionSet));
				System.out.println(i++);
			} while (s.incrementNumOfClassIndex());
			
		}

	}

	private ExperimentResult runOneExperimentConfiguration(
			ExperimentSetup setup, int averageOverXRuns, boolean generateSolutionSet)
			throws NoSuchMethodException, InterruptedException, Exception {

		long objectModelToPrologQueryTime;

		/**
		 * Build object model
		 */
		long objectModelGenerationTime = System.currentTimeMillis();
		ExperimentCoordinater.setOm(this.buildObjectModel(setup));
		objectModelGenerationTime = System.currentTimeMillis()
				- objectModelGenerationTime;

		/**
		 * Generate prolog query
		 */
		objectModelToPrologQueryTime = System.currentTimeMillis();
		ExperimentCoordinater.setPrologQuery(setup.getQueryName()
				+ this.getPrologQuery(ExperimentCoordinater.getOm(), setup, generateSolutionSet).toString());
		ExperimentCoordinater.setPrologQuery(
				"garbage_collect," + //run garbage collection before each experiment run
				//"statistics(runtime, [T0|_]),"
				ExperimentCoordinater.getPrologQuery()
				//+ ",statistics(runtime, [T1|_]),ExecutionTime is T1 - T0," +
				+",fd_statistics(resumptions,Resumptions),fd_statistics(entailments,Entailments),fd_statistics(prunings,Prunings),fd_statistics(backtracks,Backtracks),fd_statistics(constraints,Constraints).");
		objectModelToPrologQueryTime = System.currentTimeMillis()
				- objectModelToPrologQueryTime;
		if (setup.isPrintQuery()) {
			System.out.println(ExperimentCoordinater.getPrologQuery());
		}
		
		
		
		/**
		 * Evaluate Prolog query
		 */
		HashMap<String, String> stats = new HashMap<String, String>();
		ArrayList<Integer> averageExecutionTimeList = new ArrayList<>();
		for (int i = 0; i < averageOverXRuns; i++) {
			try {
				stats = this.getPrologInterface().evaluateQuery(ExperimentCoordinater.getPrologQuery(),
						setup.isPrintSolution());			
			} catch (PrologEvaluationException e) {
				System.out.println(e);
				System.out.println("PQG used: " + this);
				System.out.println("File used: " + getSimplerPrologFileName());
			}		
			averageExecutionTimeList.add(Integer.parseInt(stats.get("executiontime")));
			//System.out.println("Run: " + stats.get("executiontime") + "\n");
		}
		
		int averageExecutionTime = 0;
		if(averageOverXRuns != 1){
			averageExecutionTime = getInterquartileRangeMean(averageExecutionTimeList);
		
		} else {
			averageExecutionTime = averageExecutionTimeList.get(0);
		}
		// set statistics in experimentresult object
		setup.getExperimentResult().get(setup.getNumOfClassIndex())
				.setDependent_resumptions(stats.get("resumptions"));
		setup.getExperimentResult().get(setup.getNumOfClassIndex())
				.setDependent_entailments(stats.get("entailments"));
		setup.getExperimentResult().get(setup.getNumOfClassIndex())
				.setDependent_prunings(stats.get("prunings"));
		setup.getExperimentResult().get(setup.getNumOfClassIndex())
				.setDependent_backtracks(stats.get("backtracks"));
		setup.getExperimentResult().get(setup.getNumOfClassIndex())
				.setDependent_constraints(stats.get("constraints"));

		/**
		 * Performance
		 */

		setup.getExperimentResult()
				.get(setup.getNumOfClassIndex())
				.setDependent_objectModelToPrologQueryTime(
						objectModelToPrologQueryTime);
		setup.getExperimentResult().get(setup.getNumOfClassIndex()).setDependent_prologEvaluationTime(averageExecutionTime+"");
//		setup.getExperimentResult().get(setup.getNumOfClassIndex())
//				.setDependent_prologEvaluationTime(stats.get("executiontime"));

		/**
		 * OCL Complexity
		 */

		setup.getExperimentResult().get(setup.getNumOfClassIndex())
				.setIndependent_ComplexityOfOCL(setup.getOclComplexity());
		return setup.getExperimentResult().get(setup.getNumOfClassIndex());
	}

	private int getInterquartileRangeMean(
			ArrayList<Integer> averageExecutionTimeList) {
		int averageExecutionTime = 0;
		int averageOverXRuns = averageExecutionTimeList.size();
		Collections.sort(averageExecutionTimeList);
		int leftQuartile = (int) (averageOverXRuns*0.25);
		int rightQuartile = (int) (averageOverXRuns*0.75);
		
		for (int i = 0; i < averageOverXRuns; i++) {
			if(i < leftQuartile){
				averageExecutionTimeList.remove(0);
			} else if(i > rightQuartile) {
				averageExecutionTimeList.remove(averageExecutionTimeList.size()-1);
			}
		}
		
		for (Integer integer : averageExecutionTimeList) {
			averageExecutionTime += integer;
		}
		
		averageExecutionTime = averageExecutionTime/averageExecutionTimeList.size();
		System.out.println("Average: " + averageExecutionTime+ "\n");
		return averageExecutionTime;
	}

	/**
	 * Compiles a list of results into CSV (comma separated values) format
	 * 
	 * @param results
	 * @throws IOException
	 */
	public static void writeCSV(ArrayList<ExperimentResult> results, String fileName,
			char seperator) throws IOException {
		CSVWriter writer = new CSVWriter(new FileWriter(fileName), seperator);
		// create list of headers, assumes that all experiment results has the
		// same fields in it's class instantiation
		String[] headers = results.get(0).getCSVHeader();
		writer.writeNext(headers);
		for (ExperimentResult result : results) {
			String listAsString = 
					  result.getRefType() + "#"
					+ result.getModelRepresentation() + "#"
					+ result.getConstrainType() + "#"
					+ result.getConfiguration() + "#"
					+ result.getPredOrganization() + "#"
					+ result.getIndependent_ComplexityOfOCL() + "#"
					+ result.getTotalNumberOfClasses() + "#"
					+ result.getDependent_objectModelToPrologQueryTime() + "#"
					+ result.getDependent_prologEvaluationTime() + "#"
					+ result.getDependent_resumptions() + "#"
					+ result.getDependent_entailments() + "#"
					+ result.getDependent_prunings() + "#"
					+ result.getDependent_backtracks() + "#"
					+ result.getDependent_constraints() + "#"
					+ result.getTotalNumberOfUnconfiguredAttributes() + "#"
					+ result.getTotalNumberOfConfiguredAttributes() + "#";

			String[] list = listAsString.split("#");
			writer.writeNext(list);
		}

		writer.close();
	}

	
	/**
	 * Checks if a string is in prolog variable form
	 * 
	 * @param s
	 *            the string to check
	 * @return if the input string starts with a upper case letter
	 */
	private boolean isPrologVariable(String s) {
		return s.matches("[A-Z].+");
	}

	/**
	 * 
	 * @param s
	 *            String to check
	 * @return whether the string is either an integer or a list of integers of
	 *         type: [1,2,34,56,7] etc
	 */
	private boolean isNotNumeric(String s) {
		return !s.matches("-?\\d+")
				&& !s.matches("((?:\\[\\d+,|\\d+,|\\d+\\])+)");
	}

	/**
	 * 
	 * @return name of the implementing class in lowercase
	 */
	public String getName() {
		return this.getClass().getSimpleName().toLowerCase();
	}

	public PrologInterface getPrologInterface() {
		return prologInterface;
	}

	public ArrayList<ExperimentSetup> getSetups() {
		return setups;
	}

	public void setSetups(ArrayList<ExperimentSetup> setups) {
		this.setups = setups;
	}

	/**
	 * @return the prologFileName
	 */
	public String getPrologFileName() {
		return prologFileName;
	}

	public void setPrologFileName(String fileName) {
		this.prologFileName = fileName;
	}

	/**
	 * returns prolog file name without file ending
	 * 
	 * @return
	 */
	public String getSimplerPrologFileName() {
		String fileName = getPrologFileName();
		return fileName.substring(fileName.lastIndexOf("\\") + 1,
				fileName.indexOf(".sav"));
	}

	/**
	 * Loads new prolog code using the built in SP_restore() Takes .sav files as
	 * input. To get this file for your prolog code, compile your code and use
	 * the save_program('fileName.sav') predicate
	 * 
	 * @param prologFileLocation
	 * @throws SPException
	 * @throws PrologFileRestoreException
	 */
	public void loadPrologFile(String prologFileLocation) throws SPException,
			PrologFileRestoreException {
		if (!prologFileLocation.endsWith(".sav")) {
			throw new PrologFileRestoreException(prologFileLocation
					+ "is not a path to a prolog .sav file");
		}
		getPrologInterface().load(prologFileLocation);
		setPrologFileName(prologFileLocation);
	}

	/**
	 * 
	 * @param classSize how many object of this class
	 * @param numberOfRuns the number of runs of thix experimentsetup
	 * @return
	 */
	public static Integer[] generateClassSizeList(int classSize, int numberOfRuns) {
		Integer[] list = new Integer[numberOfRuns];
		for (int j = 0; j < numberOfRuns; j++) {
			list[j] = classSize;
		}
		return list;
	}
	
	public String toString(){
		return this.getName();
	}
}
