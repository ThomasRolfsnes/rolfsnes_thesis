package rolfsnes.cp.prologexperiments.pqgs.naive_assoc;

import java.util.ArrayList;

import rolfsnes.cp.prologexperiments.prologquerygenerator.ExperimentSetup;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PrologAttribute;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PrologClass;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PrologObject;

public class ElectronicConnection extends PrologClass {

	public ElectronicConnection(ArrayList<PrologAttribute> attributes,
			PrologClass parent, ArrayList<PrologClass> children,
			ExperimentSetup experimentSetup) {
		super(attributes, parent, children, experimentSetup);
		// TODO Auto-generated constructor stub
	}

	public ElectronicConnection(ExperimentSetup experimentSetup) {
		super(experimentSetup);
		// TODO Auto-generated constructor stub
	}

	public ElectronicConnection(String id, ExperimentSetup experimentSetup) {
		super(id, experimentSetup);
	}
	
	public StringBuilder toProlog(){
		
		StringBuilder s = new StringBuilder();
		
		//s.append("[");
		s.append(getID(false,true)+"-[");
		s.append(writeAttributes(true, true, "refs-", true, ','));
		s.append("]");
		return s;
		
	}
}
