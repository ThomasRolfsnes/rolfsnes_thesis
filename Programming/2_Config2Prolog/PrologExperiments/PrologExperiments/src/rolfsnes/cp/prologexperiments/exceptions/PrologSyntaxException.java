package rolfsnes.cp.prologexperiments.exceptions;

/**
 * Should be thrown when there is identified an input to the prolog engine that breaks prolog syntax rules.
 * For example generating a prolog variable and using lower case first letter.
 * @author ThomasRolf
 *
 */
public class PrologSyntaxException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2818806773625240442L;

	public PrologSyntaxException() {
	}

	public PrologSyntaxException(String message) {
		super(message);
	}

	public PrologSyntaxException(Throwable cause) {
		super(cause);
	}

	public PrologSyntaxException(String message, Throwable cause) {
		super(message, cause);
	}

}
