package rolfsnes.cp.prologexperiments.exceptions;

public class UniquenessException extends Exception {

	public UniquenessException() {
	}

	public UniquenessException(String message) {
		super(message);
	}

	public UniquenessException(Throwable cause) {
		super(cause);
	}

	public UniquenessException(String message, Throwable cause) {
		super(message, cause);
	}
}
