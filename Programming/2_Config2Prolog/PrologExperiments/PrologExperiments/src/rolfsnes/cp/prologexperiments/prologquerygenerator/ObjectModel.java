package rolfsnes.cp.prologexperiments.prologquerygenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Used to have a central location for all our classes
 * @author ThomasRolf
 *
 */
public class ObjectModel {

	private Map<String,ArrayList<PrologClass>> classMap;
	
	public ObjectModel(){
		this.setClassMap(new HashMap<String,ArrayList<PrologClass>>());
	}

	/**
	 * Add a class to the classMap. If the key (name of class) does not exist. A new ArrayList<PrologObject>
	 * is created at that entry.
	 * @param o PrologObject you want to add
	 * @return the key that was used when adding the class
	 */
	public String addClass(PrologClass o){
		if(this.getClassMap().get(o.getName()) != null){
			this.getClassMap().get(o.getName()).add(o);
		} else {
			ArrayList<PrologClass> list = new ArrayList<PrologClass>();
			list.add(o);
			this.getClassMap().put(o.getName(),list);
		}
		return o.getName();
	}
	
	public ArrayList<PrologClass> getClassList(String key){
		ArrayList<PrologClass> list = this.getClassMap().get(key.toLowerCase());
		if(list == null){
			throw new NullPointerException("No entry with that key (key = name of class in small caps)");
		} else {
			return list;
		}
	}
	
	
	public Map<String, ArrayList<PrologClass>> getClassMap() {
		return classMap;
	}


	public void setClassMap(Map<String, ArrayList<PrologClass>> classMap) {
		this.classMap = classMap;
	}

}
