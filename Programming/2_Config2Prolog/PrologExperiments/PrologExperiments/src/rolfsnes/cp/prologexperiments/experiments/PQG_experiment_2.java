package rolfsnes.cp.prologexperiments.experiments;

import rolfsnes.cp.prologexperiments.exceptions.OutOfBoundsException;
import rolfsnes.cp.prologexperiments.exceptions.PrologSyntaxException;
import rolfsnes.cp.prologexperiments.exceptions.UniquenessException;
import rolfsnes.cp.prologexperiments.prologquerygenerator.ExperimentSetup;
import rolfsnes.cp.prologexperiments.prologquerygenerator.ObjectModel;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PQG;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PrologInterface;
import se.sics.jasper.SPException;

public class PQG_experiment_2 extends PQG {

	

	public PQG_experiment_2(PrologInterface prologInterface) throws SPException {
		super(prologInterface);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ObjectModel buildObjectModel(ExperimentSetup e)
			throws PrologSyntaxException, UniquenessException,
			OutOfBoundsException {

		
		
		return null;
	}

	@Override
	public StringBuilder getPrologQuery(ObjectModel om, ExperimentSetup setup,
			boolean generateSolutionSet) {

		StringBuilder query = new StringBuilder();
		
		query.append("(X,Y,Z)");;
		
		if(generateSolutionSet){
			query = generateSolutionSet(setup, query);
		}
		
		return query;
	}

	
}
