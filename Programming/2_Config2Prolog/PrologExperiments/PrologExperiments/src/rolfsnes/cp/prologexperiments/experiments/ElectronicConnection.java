package rolfsnes.cp.prologexperiments.experiments;

import java.util.ArrayList;

import rolfsnes.cp.prologexperiments.prologquerygenerator.ExperimentSetup;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PQG;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PrologAttribute;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PrologClass;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PrologObject;


public class ElectronicConnection extends PrologClass{
	
	

	public ElectronicConnection(ArrayList<PrologAttribute> attributes, PrologClass parent, ArrayList<PrologClass> children,ExperimentSetup experimentSetup) {
		super(attributes, parent, children, experimentSetup);
		// TODO Auto-generated constructor stub
		
	}
	
	public ElectronicConnection(ExperimentSetup experimentSetup){
		super(experimentSetup);
	}

	public StringBuilder toProlog(){
	
		StringBuilder s = new StringBuilder();
		//s.append(getName());
		//s.append("(");
		s.append("[");
		s.append(this.getID(true, false)+",");
		for (PrologObject o: getAttributes()) {
			s.append(o.toProlog());
			if(!o.equals(getAttributes().get(getAttributes().size()-1))){
				s.append(",");				
			}
		}
		s.append("]");
		
		return s;
	}



	
	
	
}
