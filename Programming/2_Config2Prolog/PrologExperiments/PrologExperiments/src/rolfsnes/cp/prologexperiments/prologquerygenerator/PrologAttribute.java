package rolfsnes.cp.prologexperiments.prologquerygenerator;

import rolfsnes.cp.prologexperiments.exceptions.OutOfBoundsException;
import rolfsnes.cp.prologexperiments.exceptions.PrologSyntaxException;

public class PrologAttribute extends PrologObject {

	// private final boolean configurable

	private final String name;
	private final boolean configured;
	private final String configuredState;
	private final String unConfiguredState;
	
	private final boolean list;
	
	

	/**
	 * 
	 * @param configuredState
	 *            How should the attribute look like when it's configured
	 * @param unConfiguredState
	 *            How the attribute should look like when unConfigured (prolog
	 *            variable)
	 * @param parentClass
	 *            the parent class of this attribute
	 * @param e
	 *            the experimentSetup to use for this instantiation
	 * @throws PrologSyntaxException
	 *             if the UnConfiguredState does not follow prolog syntax rules
	 *             for variables.
	 * @throws OutOfBoundsException 
	 */
	public PrologAttribute(String name,String configuredState, String unConfiguredState,
			PrologClass parentClass, ExperimentSetup experimentSetup)
			throws PrologSyntaxException, OutOfBoundsException {
		super(experimentSetup);
		if (!isPrologVariable(unConfiguredState) && !isPrologVariableList(unConfiguredState)) {
			throw new PrologSyntaxException(
					unConfiguredState
							+ " is neither a prolog variable or a list of prolog variables (must start with upper case letter)");
		} else if (parentClass == null) {
			throw new NullPointerException(
					"ParentClass can't be null when creating new attribute. Was null when creating: "
							+ unConfiguredState);
		}
		
		this.name = name;
		this.unConfiguredState = unConfiguredState;
		this.configuredState = configuredState;
		this.configured = determineConfiguration(parentClass, experimentSetup);

		// increase counter for this attribute
		increaseCount();
		
		parentClass.getAttributes().add(this);
	
		this.list = isPrologList(getConfiguredState());
	}
	
	

	private boolean determineConfiguration(PrologClass parentClass,
			ExperimentSetup e) throws OutOfBoundsException {

		double configurationThreshold = e
				.getAttributeConfigurationPercentageValue(name);
		double classSize = e.getClassSize(parentClass.getName(), e.getNumOfClassIndex());

		double numOfCurrentlyConfigured = getNumberOfCurrentlyConfiguredAttributes();
				

		boolean shouldConfigure = numOfCurrentlyConfigured / classSize < configurationThreshold;

		return shouldConfigure;
	}

	

	private int getNumberOfCurrentlyConfiguredAttributes() {
		int v = 0;
		int index = getExperimentSetup().getNumOfClassIndex();
		if(getExperimentSetup().getExperimentResult().get(index).getIndependent_NumberOfConfiguredAttributes().get(name) != null){
			v = getExperimentSetup().getExperimentResult().get(index).getIndependent_NumberOfConfiguredAttributes().get(name);
		} 
		return v;
	}

	

	protected void increaseCount() {
		int index = getExperimentSetup().getNumOfClassIndex();
		if (isConfigured()) {
			if (getExperimentSetup().getExperimentResult().get(index).getIndependent_NumberOfConfiguredAttributes().get(name) == null) {
				getExperimentSetup().getExperimentResult().get(index).getIndependent_NumberOfConfiguredAttributes().put(name, 1);
			} else {
				int previous = getExperimentSetup().getExperimentResult().get(index).getIndependent_NumberOfConfiguredAttributes().get(name);
				getExperimentSetup().getExperimentResult().get(index).getIndependent_NumberOfConfiguredAttributes().put(name, previous + 1);
			}
		} else {
			if (getExperimentSetup().getExperimentResult().get(index).getIndependent_NumberOfUnConfiguredAttributes().get(name) == null) {
				getExperimentSetup().getExperimentResult().get(index).getIndependent_NumberOfUnConfiguredAttributes().put(name, 1);
			} else {
				int previous = getExperimentSetup().getExperimentResult().get(index).getIndependent_NumberOfUnConfiguredAttributes().get(name);
				getExperimentSetup().getExperimentResult().get(index).getIndependent_NumberOfUnConfiguredAttributes().put(name, previous + 1);
			}
		}

	}

	public boolean isConfigured() {
		return configured;
	}
	
	

	public String getConfiguredState() {
		return configuredState;
	}

	public String getUnConfiguredState() {
		return unConfiguredState;
	}

	/**
	 * Generates the prolog representation of this attribute
	 * @param addTypeIdentifier if true adds "type-" before attribute value. If set to false this method is the same as toProlog()
	 * @return
	 */
	public StringBuilder toProlog(boolean addTypeIdentifier) {
		StringBuilder s = new StringBuilder();
		if (addTypeIdentifier) {
			s.append(getName().toLowerCase()+"-");
		}
		if (isConfigured()) {
			s.append(getConfiguredState());
		} else {
			if(isPrologVariableList(unConfiguredState)){
				String[] varList = ((String) unConfiguredState.subSequence(1, unConfiguredState.length()-1)).split(",");
				StringBuilder varListWithIds = new StringBuilder();
				varListWithIds.append("[");
				for (String var : varList) {
					varListWithIds.append(var+generateID());
					if(var != varList[varList.length-1]){
						varListWithIds.append(",");
					}
				}
				varListWithIds.append("]");
				s.append(varListWithIds);
			} else {
				s.append(getUnConfiguredState());
				s.append(getID(false,false));				
			}
			
			//Don't attempt to find solutions for lists
			// Should add the possibilitiy for having a list of variables though...
			if(!isList()){
				int index = getExperimentSetup().getNumOfClassIndex();
				getExperimentSetup().getExperimentResult().get(index).addSolution(getUnConfiguredState());				
			}
		}
		return s;
	}

	/**
	 * Checks if a string is in prolog variable form
	 * 
	 * @param s
	 *            the string to check
	 * @return if the input string starts with a upper case letter
	 */
	private boolean isPrologVariable(String s) {
		return s.matches("[A-Z].+");
	}
	/**
	 * Checks if s is a list where each element starts with a capital letter
	 * @param s
	 * @return
	 */
	private boolean isPrologVariableList(String s){
		return s.matches("\\[((?:[A-Z]+\\w*(?:,[A-Z]+\\w*)*))+\\]");
	}
	
	private boolean isPrologList(String s) {
		return s.matches("\\[((?:\\d+|(?:\\d+(?:,\\d+)*))*|(?:\\w+|(?:\\w+(?:,\\w+)*))*)\\]");
	}

	public boolean isList() {
		return list;
	}



	



	@Override
	public StringBuilder toProlog() {
		StringBuilder s = new StringBuilder();
		
		if (isConfigured()) {
			s.append(getConfiguredState());
		} else {
			s.append(getUnConfiguredState());
			//Don't attempt to find solutions for lists
			if(!isList()){
				int index = getExperimentSetup().getNumOfClassIndex();
				getExperimentSetup().getExperimentResult().get(index).addSolution(getUnConfiguredState());				
			}
		}
		return s;
	}



	@Override
	public String getName() {
		return this.name;

	}



	

}
