/**
 * 
 */
package rolfsnes.cp.prologexperiments.pqgs.classic_term;

import java.util.ArrayList;

import rolfsnes.cp.prologexperiments.prologquerygenerator.ExperimentSetup;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PrologAttribute;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PrologClass;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PrologObject;

/**
 * @author ThomasRolf
 *
 */
public class SEM extends PrologClass {

	/**
	 * @param attributes
	 * @param parent
	 * @param children
	 * @param experimentSetup
	 */
	public SEM(ArrayList<PrologAttribute> attributes, PrologClass parent,
			ArrayList<PrologClass> children, ExperimentSetup experimentSetup) {
		super(attributes, parent, children, experimentSetup);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param experimentSetup
	 */
	public SEM(ExperimentSetup experimentSetup) {
		super(experimentSetup);
		// TODO Auto-generated constructor stub
	}
	
	public SEM(String id, ExperimentSetup experimentSetup) {
		super(id, experimentSetup);
	}

	public StringBuilder toProlog(){
		StringBuilder s = new StringBuilder();
		
		s.append("me(");
		s.append(getID(true,false)+",");
		s.append(writeAttributes(false, true, "refs-", false, ','));
		s.append(")");
		return s;
	}
}
