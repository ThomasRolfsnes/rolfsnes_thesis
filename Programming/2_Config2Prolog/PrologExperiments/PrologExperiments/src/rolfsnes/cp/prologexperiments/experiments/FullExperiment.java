package rolfsnes.cp.prologexperiments.experiments;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import rolfsnes.cp.prologexperiments.pqgs.classic_assoc.PQG_classic_assoc;
import rolfsnes.cp.prologexperiments.pqgs.classic_list.PQG_classic_list;
import rolfsnes.cp.prologexperiments.pqgs.classic_term.PQG_classic_term;
import rolfsnes.cp.prologexperiments.pqgs.naive_assoc.PQG_naive_assoc;
import rolfsnes.cp.prologexperiments.pqgs.naive_list.PQG_naive_list;
import rolfsnes.cp.prologexperiments.pqgs.naive_term.PQG_naive_term;
import rolfsnes.cp.prologexperiments.prologquerygenerator.AttributeConfiguration;
import rolfsnes.cp.prologexperiments.prologquerygenerator.Domain;
import rolfsnes.cp.prologexperiments.prologquerygenerator.ExperimentResult;
import rolfsnes.cp.prologexperiments.prologquerygenerator.ExperimentSetup;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PQG;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PrologInterface;

public class FullExperiment {

	static String prologFilesPath = "C:\\Users\\ThomasRolf\\workspace\\thesis\\rolfsnes_thesis\\Programming\\2_Config2Prolog\\SicStus\\OCLConstraintSolver\\src\\full_experiment\\";
	static String outputFolder = prologFilesPath + "output\\";
	static int runCounter = 0;

	enum setupType {
		UNCONFIGURED, CONFIGURED
	}

	enum refType {
		CONTAINED, LISTID, ASSOCID
	}

	enum constraintSet {
		INSTANCE, CLASSWIDE, COMBINED
	}

	private static int averageOverXRuns = 1;
	private static Integer[] classSizePerRun = new Integer[] {
	 3000,
	 3000,
	 3000,
	 3000,
	 3000};
//};
//	 private static Integer[] classSizePerRun = new Integer[] {
//		 1000,
//		 1000,
//		 1000,
//		 1000,
//		 1000,
//		 1000,
//		 1000,
//		 1000,
//		 1000,
//		 1000,
//		 3000,
//		 3000,
//		 3000,
//		 3000,
//		 3000,
//		 3000,
//		 3000,
//		 3000,
//		 3000,
//		 3000,
//		 5000,
//		 5000,
//		 5000,
//		 5000,
//		 5000,
//		 5000,
//		 5000,
//		 5000,
//		 5000,
//		 5000,
//		 7000,
//		 7000,
//		 7000,
//		 7000,
//		 7000,
//		 7000,
//		 7000,
//		 7000,
//		 7000,
//		 7000,
//		 9000,
//		 9000,
//		 9000,
//		 9000,
//		 9000,
//		 9000,
//		 9000,
//		 9000,
//		 9000,
//		 9000,
//		 11000,
//		 11000,
//		 11000,
//		 11000,
//		 11000,
//		 11000,
//		 11000,
//		 11000,
//		 11000,
//		 11000
//	 
//	 };

	final static JLabel numberOfrunsLabel = new JLabel(
			"Waiting for first run to complete");
	static private int numberOfRuns = 1;

	/**
	 * @param args
	 * @throws Exception
	 * @throws InterruptedException
	 * @throws NoSuchMethodException
	 */
	public static void main(String[] args) throws NoSuchMethodException,
			InterruptedException, Exception {

		JOptionPane
				.showMessageDialog(null,
						"Experiment starting, set priority of javaw to realtime in task manager");

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// Creation of a Panel to contain the JLabels
				JFrame.setDefaultLookAndFeelDecorated(true);
				JFrame frame = new JFrame("Running Experiment");

				JPanel totalGUI = new JPanel();
				totalGUI.setLayout(null);
				JPanel textPanel = new JPanel();
				textPanel.setLayout(null);
				textPanel.setLocation(10, 0);
				textPanel.setSize(260, 30);
				totalGUI.add(textPanel);
				numberOfrunsLabel.setLocation(0, 0);
				numberOfrunsLabel.setSize(260, 30);
				numberOfrunsLabel.setHorizontalAlignment(0);
				textPanel.add(numberOfrunsLabel);
				totalGUI.setOpaque(true);
				frame.setContentPane(totalGUI);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(290, 130);
				frame.setVisible(true);
			}
		});

		final ArrayList<ExperimentResult> allResults = new ArrayList<ExperimentResult>();
		// The objectmodel used/reused in experiments
		PrologInterface prologInterface = new PrologInterface();
		/**
		 * SETUP PQGS AND PATHS
		 */

		final PQG_classic_list pqg_classic_list = new PQG_classic_list(
				prologInterface);

		PQG_naive_list pqg_naive_list = new PQG_naive_list(prologInterface);

		final PQG_classic_assoc pqg_classic_assoc = new PQG_classic_assoc(
				prologInterface);
		final PQG_naive_assoc pqg_naive_assoc = new PQG_naive_assoc(
				prologInterface);

		// PATHS
		final String list_linear_condensed = prologFilesPath
				+ "list_linear_condensed.sav";
		final String list_linear_normal = prologFilesPath
				+ "list_linear_normal.sav";
		final String list_nonlinear_condensed = prologFilesPath
				+ "list_nonlinear_condensed.sav";
		final String list_nonlinear_normal = prologFilesPath
				+ "list_nonlinear_normal.sav";

		final String list_combined_normal = prologFilesPath
				+ "list_combined_normal.sav";
		final String list_combined_condensed = prologFilesPath
				+ "list_combined_condensed.sav";

		final String assoc_linear_normal = prologFilesPath
				+ "assoc_linear_normal.sav";
		final String assoc_linear_condensed = prologFilesPath
				+ "assoc_linear_condensed.sav";
		final String assoc_nonlinear_normal = prologFilesPath
				+ "assoc_nonlinear_normal.sav";
		final String assoc_nonlinear_condensed = prologFilesPath
				+ "assoc_nonlinear_condensed.sav";

		final String assoc_combined_normal = prologFilesPath
				+ "assoc_combined_normal.sav";
		final String assoc_combined_condensed = prologFilesPath
				+ "assoc_combined_condensed.sav";

		/**
		 * RUN EXPERIMENTS
		 */

		long startTime = System.currentTimeMillis();
		
		/**
		 * Seperate efficiency experiment
		 */
		final String assoc_efficiency = prologFilesPath + "efficiency_comparison.sav";
		
		run_setup("naive	assoc	linear	configured	normal", pqg_classic_assoc, assoc_efficiency, setupType.CONFIGURED, allResults);
//		// naive assoc linear configured normal
//		run_setup("naive	assoc	linear	configured	normal", pqg_naive_assoc,
//				assoc_linear_normal, setupType.CONFIGURED, allResults);
//
//		// naive assoc linear configured condensed
//		run_setup("naive	assoc	linear	configured	condensed", pqg_naive_assoc,
//				assoc_linear_condensed, setupType.CONFIGURED, allResults);
//
//		// naive assoc linear unconfigured normal
//		run_setup("naive	assoc	linear	unconfigured	normal", pqg_naive_assoc,
//				assoc_linear_normal, setupType.UNCONFIGURED, allResults);
//
//		// naive assoc linear unconfigured condensed
//		run_setup("naive	assoc	linear	unconfigured	condensed", pqg_naive_assoc,
//				assoc_linear_condensed, setupType.UNCONFIGURED, allResults);
//
//		// naive assoc nonlinear configured normal
//		run_setup("naive	assoc	nonlinear	configured	normal", pqg_naive_assoc,
//				assoc_nonlinear_normal, setupType.CONFIGURED, allResults);
//
//		// naive assoc nonlinear configured condensed
//		run_setup("naive	assoc	nonlinear	configured	condensed",
//				pqg_naive_assoc, assoc_nonlinear_condensed,
//				setupType.CONFIGURED, allResults);
//
//		// naive assoc nonlinear unconfigured normal
//		run_setup("naive	assoc	nonlinear	unconfigured	normal", pqg_naive_assoc,
//				assoc_nonlinear_normal, setupType.UNCONFIGURED, allResults);
//
//		// naive assoc nonlinear unconfigured condensed
//		run_setup("naive	assoc	nonlinear	unconfigured	condensed",
//				pqg_naive_assoc, assoc_nonlinear_condensed,
//				setupType.UNCONFIGURED, allResults);
//
//		// naive assoc combined configured normal
//		run_setup("naive	assoc	combined	configured	normal", pqg_naive_assoc,
//				assoc_combined_normal, setupType.CONFIGURED, allResults);
//
//		// naive assoc combined configured condensed
//		run_setup("naive	assoc	combined	configured	condensed", pqg_naive_assoc,
//				assoc_combined_condensed, setupType.CONFIGURED, allResults);
//
//		// naive assoc combined unconfigured normal
//		run_setup("naive	assoc	combined	unconfigured	normal", pqg_naive_assoc,
//				assoc_combined_normal, setupType.UNCONFIGURED, allResults);
//
//		// naive assoc combined unconfigured condensed
//		run_setup("naive	assoc	combined	unconfigured	condensed",
//				pqg_naive_assoc, assoc_combined_condensed,
//				setupType.UNCONFIGURED, allResults);
//
//		run_setup("classic	assoc	linear	configured	normal", pqg_classic_assoc,
//				assoc_linear_normal, setupType.CONFIGURED, allResults);
//
//		// classic assoc linear configured condensed
//		run_setup("classic	assoc	linear	configured	condensed",
//				pqg_classic_assoc, assoc_linear_condensed,
//				setupType.CONFIGURED, allResults);
//
//		// classic assoc linear unconfigured normal
//		run_setup("classic	assoc	linear	unconfigured	normal",
//				pqg_classic_assoc, assoc_linear_normal, setupType.UNCONFIGURED,
//				allResults);
//
//		// classic assoc linear unconfigured condensed
//		run_setup("classic	assoc	linear	unconfigured	condensed",
//				pqg_classic_assoc, assoc_linear_condensed,
//				setupType.UNCONFIGURED, allResults);
//
//		// classic assoc nonlinear configured normal
//		run_setup("classic	assoc	nonlinear	configured	normal",
//				pqg_classic_assoc, assoc_nonlinear_normal,
//				setupType.CONFIGURED, allResults);
//
//		// classic assoc nonlinear configured condensed
//		run_setup("classic	assoc	nonlinear	configured	condensed",
//				pqg_classic_assoc, assoc_nonlinear_condensed,
//				setupType.CONFIGURED, allResults);
//
//		// classic assoc nonlinear unconfigured normal
//		run_setup("classic	assoc	nonlinear	unconfigured	normal",
//				pqg_classic_assoc, assoc_nonlinear_normal,
//				setupType.UNCONFIGURED, allResults);
//
//		// classic assoc nonlinear unconfigured condensed
//		run_setup("classic	assoc	nonlinear	unconfigured	condensed",
//				pqg_classic_assoc, assoc_nonlinear_condensed,
//				setupType.UNCONFIGURED, allResults);
//
//		// classic assoc combined configured normal
//		run_setup("classic	assoc	combined	configured	normal",
//				pqg_classic_assoc, assoc_combined_normal, setupType.CONFIGURED,
//				allResults);
//
//		// classic assoc combined configured condensed
//		run_setup("classic	assoc	combined	configured	condensed",
//				pqg_classic_assoc, assoc_combined_condensed,
//				setupType.CONFIGURED, allResults);
//
//		// classic assoc combined unconfigured normal
//		run_setup("classic	assoc	combined	unconfigured	normal",
//				pqg_classic_assoc, assoc_combined_normal,
//				setupType.UNCONFIGURED, allResults);
//
//		// classic assoc combined unconfigured condensed
//		run_setup("classic	assoc	combined	unconfigured	condensed",
//				pqg_classic_assoc, assoc_combined_condensed,
//				setupType.UNCONFIGURED, allResults);
//
//		// naive list linear configured normal
//		run_setup("naive		list		linear		configured		normal", pqg_naive_list,
//				list_linear_normal, setupType.CONFIGURED, allResults);
//
//		// naive list linear configured condensed
//		run_setup("naive		list		linear		configured		condensed", pqg_naive_list,
//				list_linear_condensed, setupType.CONFIGURED, allResults);
//
//		// naive list linear unconfigured normal
//		run_setup("naive		list		linear		unconfigured		normal", pqg_naive_list,
//				list_linear_normal, setupType.UNCONFIGURED, allResults);
//
//		// naive list linear unconfigured condensed
//		run_setup("naive		list		linear		unconfigured		condensed",
//				pqg_naive_list, list_linear_condensed, setupType.UNCONFIGURED,
//				allResults);
//
//		// naive list nonlinear configured normal
//		run_setup("naive		list		nonlinear		configured		normal", pqg_naive_list,
//				list_nonlinear_normal, setupType.CONFIGURED, allResults);
//
//		// naive list nonlinear configured condensed
//		run_setup("naive		list		nonlinear		configured		condensed",
//				pqg_naive_list, list_nonlinear_condensed, setupType.CONFIGURED,
//				allResults);
//
//		// naive list nonlinear unconfigured normal
//		run_setup("naive		list		nonlinear		unconfigured		normal",
//				pqg_naive_list, list_nonlinear_normal, setupType.UNCONFIGURED,
//				allResults);
//
//		// naive list nonlinear unconfigured condensed
//		run_setup("naive		list		nonlinear		unconfigured		condensed",
//				pqg_naive_list, list_nonlinear_condensed,
//				setupType.UNCONFIGURED, allResults);
//
//		// naive list combined configured normal
//		run_setup("naive		list		combined		configured		normal", pqg_naive_list,
//				list_combined_normal, setupType.CONFIGURED, allResults);
//
//		// naive list combined configured condensed
//		run_setup("naive		list		combined		configured		condensed",
//				pqg_naive_list, list_combined_condensed, setupType.CONFIGURED,
//				allResults);
//
//		// naive list combined unconfigured normal
//		run_setup("naive		list		combined		unconfigured		normal",
//				pqg_naive_list, list_combined_normal, setupType.UNCONFIGURED,
//				allResults);
//
//		// naive list combined unconfigured condensed
//		run_setup("naive		list		combined		unconfigured		condensed",
//				pqg_naive_list, list_combined_condensed,
//				setupType.UNCONFIGURED, allResults);
//
//		// classic list linear configured normal
//		run_setup("classic		list		linear		configured		normal",
//				pqg_classic_list, list_linear_normal, setupType.CONFIGURED,
//				allResults);
//
//		// classic list linear configured condensed
//		run_setup("classic		list		linear		configured		condensed",
//				pqg_classic_list, list_linear_condensed, setupType.CONFIGURED,
//				allResults);
//
//		// classic list linear unconfigured normal
//		run_setup("classic		list		linear		unconfigured		normal",
//				pqg_classic_list, list_linear_normal, setupType.UNCONFIGURED,
//				allResults);
//
//		// classic list linear unconfigured condensed
//		run_setup("classic		list		linear		unconfigured		condensed",
//				pqg_classic_list, list_linear_condensed,
//				setupType.UNCONFIGURED, allResults);
//
//		// classic list nonlinear configured normal
//		run_setup("classic		list		nonlinear		configured		normal",
//				pqg_classic_list, list_nonlinear_normal, setupType.CONFIGURED,
//				allResults);
//
//		// classic list nonlinear configured condensed
//		run_setup("classic		list		nonlinear		configured		condensed",
//				pqg_classic_list, list_nonlinear_condensed,
//				setupType.CONFIGURED, allResults);
//
//		// classic list nonlinear unconfigured normal
//		run_setup("classic		list		nonlinear		unconfigured		normal",
//				pqg_classic_list, list_nonlinear_normal,
//				setupType.UNCONFIGURED, allResults);
//
//		// classic list nonlinear unconfigured condensed
//		run_setup("classic		list		nonlinear		unconfigured		condensed",
//				pqg_classic_list, list_nonlinear_condensed,
//				setupType.UNCONFIGURED, allResults);
//
//		// classic list combined configured normal
//		run_setup("classic		list		combined		configured		normal",
//				pqg_classic_list, list_combined_normal, setupType.CONFIGURED,
//				allResults);
//
////		// classic list combined configured condensed
//		run_setup("classic		list		combined		configured		condensed",
//				pqg_classic_list, list_combined_condensed,
//				setupType.CONFIGURED, allResults);
//
//		// classic list combined unconfigured normal
//		run_setup("classic		list		combined		unconfigured		normal",
//				pqg_classic_list, list_combined_normal, setupType.UNCONFIGURED,
//				allResults);
//
//		// classic list combined unconfigured condensed
//		run_setup("classic		list		combined		unconfigured		condensed",
//				pqg_classic_list, list_combined_condensed,
//				setupType.UNCONFIGURED, allResults);

		/**
		 * WRITE RESULTS TO CSV
		 */
		PQG.writeCSV(allResults, outputFolder + "full.csv", ',');

		long endTime = System.currentTimeMillis();
		long total = (endTime - startTime) / 1000;
		long seconds = total % 60;
		long minutes = (total - seconds) / 60;
		System.out.println("\n\nEXPERIMENT DONE! Used " + minutes
				+ " minutes and " + seconds + " seconds.");

	}

	private static void run_setup(String label, PQG pqg, String prologFilePath,
			setupType configuration,
			ArrayList<ExperimentResult> totalResultArray)
			throws NoSuchMethodException, InterruptedException, Exception {

		switch (configuration) {
		case CONFIGURED:
			ExperimentCoordinater.setSetups(getSetups_configured(false));
			break;
		case UNCONFIGURED:
			ExperimentCoordinater.setSetups(getSetups_unconfigured(false));
			break;
		}
		pqg.loadPrologFile(prologFilePath);
		//pqg.setSetups(setups);

		pqg.Run(averageOverXRuns, true,
				false);

		for (ExperimentResult experimentResult : ExperimentCoordinater.getResults()) {
			setResultAttributes(experimentResult, label);

		}

		totalResultArray.addAll(ExperimentCoordinater.getResults());
		PQG.writeCSV(ExperimentCoordinater.getResults(), outputFolder + prettify(label) + ".csv", ',');

		increaseNumberOfRuns();
		
		ExperimentCoordinater.setOm(null);
		ExperimentCoordinater.setPrologQuery("");
		ExperimentCoordinater.getResults().clear();
		ExperimentCoordinater.getSetups().clear();
		System.gc();

	}

	private static void setResultAttributes(ExperimentResult experimentResult,
			String label) throws Exception {

		/**
		 * decided to change the name of some attributes, so instead of renaming
		 * everything im just dooing a lazy conversion here..
		 */
		// "naive		term		linear		unconfigured		normal"
		String[] listOfAttrs = label.split("\\s+");
		if (listOfAttrs[0].equals("naive")) {
			experimentResult.setRefType("contained");
		} else {
			experimentResult.setRefType("id");
		}
		experimentResult.setModelRepresentation(listOfAttrs[1]);
		if (listOfAttrs[2].equals("linear")) {
			experimentResult.setConstrainType("instance");
		} else if(listOfAttrs[2].equals("nonlinear")){
			experimentResult.setConstrainType("class");
		} else if(listOfAttrs[2].equals("combined")){
			experimentResult.setConstrainType("combined");
		} 
		experimentResult.setConfiguration(listOfAttrs[3]);
		experimentResult.setPredOrganization(listOfAttrs[4]);
		experimentResult.setIndependent_ComplexityOfOCL(getComplexity(label,
				experimentResult));

	}

	/**
	 * 
	 * @param generateOutput
	 * @return
	 */
	private static Collection<? extends ExperimentSetup> getSetups_configured(
			boolean generateOutput) {
		ArrayList<ExperimentSetup> setupList = new ArrayList<ExperimentSetup>();
		Map<String, Integer[]> classSize10 = new HashMap<String, Integer[]>();
		classSize10.put(PQG_rolf.ec, getClassSizePerRun());
		classSize10.put(PQG_rolf.sem, getClassSizePerRun());

		Map<String, AttributeConfiguration> attributeMap10 = new HashMap<String, AttributeConfiguration>();

		ExperimentSetup s0 = new ExperimentSetup("check_system", 0, 23,
				classSize10, attributeMap10, generateOutput, generateOutput);

		AttributeConfiguration sem_eboards = new AttributeConfiguration(
				PQG_rolf.sem_eboards, "[E1,E2,E3,E4]", new Domain(
						"[8,16,32,64]"), 1.0);

		AttributeConfiguration sem_size = new AttributeConfiguration(
				PQG_rolf.sem_Size, PQG_rolf.sem_Size, new Domain(4, 4), 1.0);

		AttributeConfiguration ec_ebindex = new AttributeConfiguration(
				PQG_rolf.ec_ebindex, PQG_rolf.ec_ebindex, new Domain(4, 4), 1.0);

		AttributeConfiguration ec_pinindex = new AttributeConfiguration(
				PQG_rolf.ec_pindex, PQG_rolf.ec_pindex, new Domain(63, 63), 1.0);

		attributeMap10.put(sem_eboards.getName(), sem_eboards);
		attributeMap10.put(sem_size.getName(), sem_size);
		attributeMap10.put(ec_ebindex.getName(), ec_ebindex);
		attributeMap10.put(ec_pinindex.getName(), ec_pinindex);

		setupList.add(s0);

		return setupList;
	}

	private static Collection<? extends ExperimentSetup> getSetups_unconfigured(
			boolean generateOutput) {
		ArrayList<ExperimentSetup> setupList = new ArrayList<ExperimentSetup>();
		Map<String, Integer[]> classSize10 = new HashMap<String, Integer[]>();
		classSize10.put(PQG_rolf.ec, getClassSizePerRun());
		classSize10.put(PQG_rolf.sem, getClassSizePerRun());

		Map<String, AttributeConfiguration> attributeMap10 = new HashMap<String, AttributeConfiguration>();

		ExperimentSetup s0 = new ExperimentSetup("check_system", 0, 23,
				classSize10, attributeMap10, generateOutput, generateOutput);

		AttributeConfiguration sem_eboards = new AttributeConfiguration(
				PQG_rolf.sem_eboards, "[E,E,E,E]", new Domain("[8,16,32,64]"),
				0.0);

		AttributeConfiguration sem_size = new AttributeConfiguration(
				PQG_rolf.sem_Size, PQG_rolf.sem_Size, new Domain(4, 4), 1.0);

		AttributeConfiguration ec_ebindex = new AttributeConfiguration(
				PQG_rolf.ec_ebindex, PQG_rolf.ec_ebindex, new Domain(4, 4), 0.0);

		AttributeConfiguration ec_pinindex = new AttributeConfiguration(
				PQG_rolf.ec_pindex, PQG_rolf.ec_pindex, new Domain(63, 63), 0.0);

		attributeMap10.put(sem_eboards.getName(), sem_eboards);
		attributeMap10.put(sem_size.getName(), sem_size);
		attributeMap10.put(ec_ebindex.getName(), ec_ebindex);
		attributeMap10.put(ec_pinindex.getName(), ec_pinindex);

		setupList.add(s0);

		return setupList;
	}

	/**
	 * @return the classSizePerRun
	 */
	public static Integer[] getClassSizePerRun() {
		
		return range(1,160000,1000);
	}

	public static Integer[] range(int start, int end, int jump) {
		int listSize = ((end - start)/jump)+1;
	    Integer[] range = new Integer[listSize];
	    
	    for (int i = start; i < listSize; i++) {
	        range[i - start] = i*jump;
	    }
	    return range;
	}
	
	/**
	 * @param classSizePerRun
	 *            the classSizePerRun to set
	 */
	public void setClassSizePerRun(Integer[] classSizePerRun) {
		this.classSizePerRun = classSizePerRun;
	}

	static public String prettify(String s) {
		return s.replaceAll("\\s+", "_");
	}

	public static long getComplexity(String label,
			ExperimentResult experimentResult) throws Exception {
		String[] attrs = label.split("\\s+");

		long objectSize = experimentResult.getTotalNumberOfClasses() / 2;

		constraintSet c;
		refType refType = null;
		if (attrs[2].equals("linear")) {
			c = constraintSet.INSTANCE;
		} else if (attrs[2].equals("nonlinear")) {
			c = constraintSet.CLASSWIDE;
		} else if (attrs[2].equals("combined")) {
			c = constraintSet.COMBINED;
		} else {
			throw new Exception(attrs[2]);
		}
		if (attrs[1].equals("assoc")) {
			refType = refType.ASSOCID;
		} else if (attrs[0].equals("naive")) {
			refType = refType.CONTAINED;
		} else if (attrs[0].equals("classic")) {
			refType = refType.LISTID;
		} else {
			throw new Exception(attrs[1] + " " + attrs[0]);
		}

		long complexity = 0;
		switch (c) {
		case INSTANCE: {
			switch (refType) {
			case CONTAINED:
				complexity = 4 * objectSize;
				break;
			case LISTID:
				complexity = 2 * objectSize + 2 * objectSize * objectSize;
				break;
			case ASSOCID:
				complexity = 2 * objectSize + 2 * objectSize * log2(objectSize);
				break;
			}
			break;
		}
		case CLASSWIDE: {
			switch (refType) {
			case CONTAINED:
				complexity = 3 * objectSize + objectSize + objectSize * log2(objectSize);
				break;
			case LISTID:
				complexity = 2 * objectSize + objectSize + objectSize * objectSize +
						objectSize* objectSize * log2(objectSize);
				break;
			case ASSOCID:
				complexity = 2 * objectSize + objectSize + objectSize * log2(objectSize)
						* log2(objectSize) + objectSize * log2(objectSize)
												;
				break;
			}
			break;
		}
		case COMBINED:
			switch (refType) {
			case CONTAINED:
				complexity = 3 * objectSize + objectSize * log2(objectSize)
						+ objectSize + 4 * objectSize;
				break;
			case LISTID:
				complexity = 2 * objectSize + objectSize * objectSize
						* log2(objectSize) + objectSize * objectSize
						+ objectSize + 2 * objectSize + 2 * objectSize * objectSize;
				break;
			case ASSOCID:
				complexity = 2 * objectSize + objectSize * log2(objectSize)
						* log2(objectSize) + objectSize * log2(objectSize)
						+ objectSize + 2 * objectSize + 2 * objectSize * log2(objectSize);
				break;
			}
			break;
			
		}
		return complexity;
	}

	static public long log2(long objectSize) {
		return (long) (Math.log(objectSize) / Math.log(2));
	}

	static void increaseNumberOfRuns() {
		numberOfrunsLabel.setText("Runs complexted: " + numberOfRuns++
				+ " of 48");
	}

}
