package rolfsnes.cp.prologexperiments.prologquerygenerator;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import rolfsnes.cp.prologexperiments.exceptions.UniquenessException;

public class PrologClass extends PrologObject {

	private ArrayList<PrologAttribute> attributes = new ArrayList<PrologAttribute>();
	private PrologClass parent;
	private ArrayList<PrologClass> children = new ArrayList<PrologClass>();

	public PrologClass(ArrayList<PrologAttribute> attributes,
			PrologClass parent, ArrayList<PrologClass> children,
			ExperimentSetup experimentSetup) {
		super(experimentSetup);

		this.setParent(parent);
		this.setChildren(children);

		increaseCount();

	}

	public PrologClass(ExperimentSetup experimentSetup) {
		super(experimentSetup);
		increaseCount();
	}

	public PrologClass(String id, ExperimentSetup experimentSetup) {
		super(id, experimentSetup);
		increaseCount();
	}

	public void setGlobalClassIdentifier() {

	}

	/**
	 * 
	 * @param attribute
	 * @throws UniquenessException
	 */
	public void addAttribute(PrologAttribute attribute)
			throws UniquenessException {
		if (getAttributes().contains(attribute)) {
			throw new UniquenessException("The attribute: " + attribute
					+ " has already been added to the attributelist of " + this);
		}
		getAttributes().add(attribute);
	}

	public ArrayList<PrologAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(ArrayList<PrologAttribute> attributes) {
		this.attributes = attributes;
	}

	public PrologClass getParent() {
		return parent;
	}

	/**
	 * Set the parent of this class. And add it to the parents set of childs (if
	 * it is not already included)
	 * 
	 * @param parent
	 */
	public void setParent(PrologClass parent) {
		if (parent == null) {
			throw new NullPointerException("Input parent class is null");
		}
		this.parent = parent;
		if (!parent.getChildren().contains(this)) {
			parent.getChildren().add(this);
		}

	}

	@Override
	protected void increaseCount() {
		ExperimentResult experimentResult = getExperimentSetup()
				.getExperimentResult().get(
						getExperimentSetup().getNumOfClassIndex());
		if (experimentResult.getIndependent_NumberOfClasses().get(getName()) == null) {
			experimentResult.getIndependent_NumberOfClasses().put(getName(), 1);
		} else {
			int previous = experimentResult.getIndependent_NumberOfClasses()
					.get(getName());
			experimentResult.getIndependent_NumberOfClasses().put(getName(),
					previous + 1);
		}

	}

	@Override
	public StringBuilder toProlog() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Adds a prologclass as a child and attribute of this class
	 * 
	 * @param child
	 *            the class to add
	 */
	public void addChild(PrologClass child) {
		getChildren().add(child);
		// getAttributes().add(child);
	}

	/**
	 * @return the children
	 */
	public ArrayList<PrologClass> getChildren() {
		return children;
	}

	/**
	 * @param children
	 *            the children to set
	 */
	public void setChildren(ArrayList<PrologClass> children) {
		this.children = children;
	}

	/**
	 * 
	 * @param containedAssociations
	 * @param addTypeIdentifier
	 * @param associationIdentifier
	 * @param seperator
	 * @return
	 */
	public StringBuilder writeAttributes(boolean containedAssociations,
			boolean addTypeIdentifier, String associationIdentifier, boolean addClassIdentifier,
			Character seperator) {
		StringBuilder s = new StringBuilder();

		// write attributes
		for (PrologAttribute o : getAttributes()) {

			s.append(o.toProlog(addTypeIdentifier));
			s.append(seperator);
			
		}
		
		//write associations
		s.append(associationIdentifier+"[");
		for(PrologClass o : getChildren()){
				if (containedAssociations) {
					s.append(o.getName()+"-" + o.toProlog(containedAssociations));					
				} else {
					s.append(o.getName()+"-" + o.getID(false,addClassIdentifier));
				}
				if (!o.equals(getChildren().get(getChildren().size() - 1))) {
					s.append(seperator);
				}
		}
		s.append("]");
		return s;
	}

	@Override
	public StringBuilder toProlog(boolean addTypeIdentifier) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getName() {
		return this.getClass().getSimpleName().toLowerCase();
	}

}
