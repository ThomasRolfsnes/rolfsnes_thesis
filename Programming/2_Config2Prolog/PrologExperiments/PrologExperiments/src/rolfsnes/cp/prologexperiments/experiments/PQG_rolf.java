package rolfsnes.cp.prologexperiments.experiments;

import java.util.ArrayList;
import java.util.Random;

import rolfsnes.cp.prologexperiments.exceptions.OutOfBoundsException;
import rolfsnes.cp.prologexperiments.exceptions.PrologSyntaxException;
import rolfsnes.cp.prologexperiments.exceptions.UniquenessException;
import rolfsnes.cp.prologexperiments.prologquerygenerator.ExperimentSetup;
import rolfsnes.cp.prologexperiments.prologquerygenerator.ObjectModel;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PQG;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PrologInterface;
import rolfsnes.cp.prologexperiments.prologquerygenerator.PrologObject;
import se.sics.jasper.SPException;

public class PQG_rolf extends PQG {

	public PQG_rolf(PrologInterface prologInterface) throws SPException {
		super(prologInterface);
		// TODO Auto-generated constructor stub
	}

	public static final String sem = SEM.class.getSimpleName().toLowerCase();
	public static final String sem_eboards = "Eboards";
	public static final String sem_Size = "Size";

	public static final String ec = ElectronicConnection.class.getSimpleName()
			.toLowerCase();
	public static final String ec_ebindex = "EbIndex";
	public static final String ec_pindex = "PinIndex";

	

	public ObjectModel buildObjectModel(ExperimentSetup e)
			throws PrologSyntaxException, UniquenessException,
			OutOfBoundsException {
		Random r = new Random();
		ObjectModel o = new ObjectModel();

		for (int i = 0; i < e.getClassSize(sem, e.getNumOfClassIndex()); i++) {
			SEM sem = new SEM(e);

			e.generatePrologAttribute(sem_eboards, sem);
			e.generatePrologAttribute(sem_Size, sem);

			o.addClass(sem);
		}

		for (int i1 = 0; i1 < e.getClassSize(ec, e.getNumOfClassIndex()); i1++) {
			ElectronicConnection ec = new ElectronicConnection(e);

			e.generatePrologAttribute(ec_ebindex, ec);
			e.generatePrologAttribute(ec_pindex, ec);

			SEM s = (SEM) o.getClassList(sem).get(i1);
					//r.nextInt(o.getClassList(sem).size()));

			ec.addChild(s);
			o.addClass(ec);
		}
		return o;
	}

	public StringBuilder getPrologQuery(ObjectModel om, ExperimentSetup setup, boolean generateSolutionSet) {
		StringBuilder query = new StringBuilder();

		if (om == null) {
			throw new NullPointerException(
					"ObjectModel must be built first! (currently null)");
		} else {

			// Create the rest of the query
			query.append("([");
			for (PrologObject o : om.getClassList(PQG_rolf.ec)) {// getElectronicConnections())
																	// {
				if (o.getClass() == ElectronicConnection.class) {
					query.append(o.toProlog()).append(",");
				}
			}
			// remove excessive comma and add closing bracket
			query.setCharAt(query.length() - 1, ']');
			query.append(",[");
			
			for (PrologObject o : om.getClassList(PQG_rolf.sem)) {
				query.append(o.toProlog()).append(",");
			}
			
			// remove excessive comma and add closing bracket
			query.setCharAt(query.length() - 1, ']');
			query.append(")");
			if (generateSolutionSet) {
				/**
				 * Solution set
				 */
				int index = setup.getNumOfClassIndex();
				if (setup.getExperimentResult().get(index).getSolutionSet()
						.length() != 0) {
					query.append(",");
					query.append(setup.getExperimentResult().get(index)
							.getSolutionSet());
					setup.getExperimentResult().get(index).getSolutionSet()
					.setLength(0);
				}
			}
			
			//query.append(".");				
		
		}
		return query;
	}

	

}
