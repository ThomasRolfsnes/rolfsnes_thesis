package rolfsnes.cp.prologexperiments.prologquerygenerator;

import java.util.HashMap;

import rolfsnes.cp.prologexperiments.exceptions.PrologEvaluationException;
import se.sics.jasper.IllegalTermException;
import se.sics.jasper.SICStus;
import se.sics.jasper.SPException;
import se.sics.jasper.SPQuery;
import se.sics.jasper.SPTerm;


public final class PrologInterface {
	
	private final SICStus sp;
	private final HashMap<String,SPTerm> valMap;
	private String currentFile;
	
	
	public PrologInterface() throws SPException{
		sp = new SICStus();
		valMap = new HashMap<String,SPTerm>();
	}
	/**
	 * 
	 * @param pl_file_location
	 * @throws SPException
	 */
	public PrologInterface(String pl_file_location) throws SPException{
		sp = new SICStus();
		valMap = new HashMap<String,SPTerm>();
		sp.restore(pl_file_location);
		currentFile = pl_file_location;
		
	}
	
	
	

	/**
	 * Iterates through the list of queries, and closes them after evaluaion.
	 * @param query to evaluate
	 * @param printSolution if solution should be printed
	 * @return hashmap with statistics of the evaluation
	 * @throws NoSuchMethodException
	 * @throws InterruptedException
	 * @throws Exception
	 */
	public HashMap<String, String> evaluateQuery(String query,boolean printSolution) throws NoSuchMethodException, InterruptedException, Exception{
		load(currentFile);
 		HashMap<String,String> stats = new HashMap<String,String>();

		// Always synchronize over creation and closing of SPQuery objects
	     synchronized (sp) {
	         // Create a dummy query that invokes true/0
	         SPQuery context = sp.openQuery("user","true",new SPTerm[]{});
	         // All SP_term_refs created after this point will be reclaimed by
	         // Prolog when doing context.close() (or context.cut())
	     
	         try {           // ensure context is always closed
	        	 if(!sp.query(query, valMap)){
	 				throw new PrologEvaluationException("The following query evaluated to false:" + query +
	 						"\n Check your query for errors.");
	 			}	
	        	 if(printSolution){
	     			printSolution();
	     		}
	     			     		
	     		stats.put("executiontime", valMap.get("ExecutionTime").toString());
	     		stats.put("resumptions", valMap.get("Resumptions").toString());
	     		stats.put("entailments", valMap.get("Entailments").toString());
	     		stats.put("prunings", valMap.get("Prunings").toString());
	     		stats.put("backtracks", valMap.get("Backtracks").toString());
	     		stats.put("constraints", valMap.get("Constraints").toString());
	         }
	         finally {
	             // This will invalidate tmp and make Prolog
	             // reclaim the corresponding SP_term_ref
	             context.close(); // or context.cut() to retain variable bindings.
	         }
	     }
	     
//		synchronized (query) {
//			if(!sp.query(query, valMap)){
//				throw new PrologEvaluationException("The following query evaluated to false:" + query +
//						"\n Check your query for errors.");
//				
//			}			
//		}
//		if(printSolution){
//			printSolution();
//		}
//		
//		HashMap<String,String> stats = new HashMap<String,String>();
//		
//		stats.put("executiontime", valMap.get("ExecutionTime").toString());
//		stats.put("resumptions", valMap.get("Resumptions").toString());
//		stats.put("entailments", valMap.get("Entailments").toString());
//		stats.put("prunings", valMap.get("Prunings").toString());
//		stats.put("backtracks", valMap.get("Backtracks").toString());
//		stats.put("constraints", valMap.get("Constraints").toString());
		
		return stats;
	}

	/**
	 * Loops through the Map of solutions, and prints all domains
	 * @throws SPException
	 */
	private void printSolution() throws SPException {
		for (String s : valMap.keySet()) {
			if(s.matches("\\S+_dom")){
				System.out.println(s + " : " + valMap.get(s));		
			}
		}
	}


	/**
	 * Clears the map that stores all variable instantiations
	 * @throws SPException 
	 * @throws IllegalTermException 
	 */
	public void clear() throws IllegalTermException, SPException {
		valMap.clear();
		
	}

	/**
	 * Loads prolog code from sav file. Unloads all foreign resources (clean slate)
	 * @param pl_file_location
	 * @throws SPException
	 */
	public void load(String pl_file_location) throws SPException{
		clear();
		this.sp.restore(pl_file_location);
		currentFile = pl_file_location;
	}
	
}
