package rolfsnes.cp.prologexperiments.prologquerygenerator;

import java.util.ArrayList;
import java.util.Map;

import rolfsnes.cp.prologexperiments.exceptions.OutOfBoundsException;
import rolfsnes.cp.prologexperiments.exceptions.PrologSyntaxException;

public class ExperimentSetup {

	// private final int numberOfRuns;
	private final String queryName;

	private final Map<String, Integer[]> numberOfClassesMap;
	private int numOfClassIndex = 0;
	private final Map<String, AttributeConfiguration> attributeConfigurationMap;

	private final ArrayList<ExperimentResult> experimentResult = new ArrayList<ExperimentResult>();

	private final int oclComplexity;

	private boolean printSolution;
	private boolean printQuery;

	/**
	 * @param queryName
	 *            name of the query to run (prolog predicate name). This
	 *            predicate should exist in the prolog file you are inputing
	 * @param complexityMultiplier
	 *            the number of copies of your constraints
	 * @param oclComplexityBase
	 *            the base complexity of your constraints
	 * @param numberOfClassesMap
	 *            Map the name of a class to the desired instantiations of that
	 *            class
	 * @param attributeConfiguration
	 *            a value between 0 and 1 (inclusive), where 0 indicated that
	 *            all attributes of this type will be unconfigured. If set to 1,
	 *            all attributes of this type will be configured.
	 * @param printSolution
	 *            if the result of the prolog evaluation should be printed
	 * @param printQuery
	 *            if the generated prolog query should be printed
	 */
	public ExperimentSetup(String queryName, int complexityMultiplier,
			int oclComplexityBase, Map<String, Integer[]> numberOfClassesMap,
			Map<String, AttributeConfiguration> attributeConfigurationMap,
			// int numberOfRuns,
			boolean printSolution, boolean printQuery) {
		if(complexityMultiplier != 0){
			this.queryName = queryName + "_" + complexityMultiplier;			
		} else {			
			this.queryName = queryName;			
		}
		this.oclComplexity = complexityMultiplier * oclComplexityBase;
		this.numberOfClassesMap = numberOfClassesMap;
		this.attributeConfigurationMap = attributeConfigurationMap;
		// this.numberOfRuns = numberOfRuns;
		this.printSolution = printSolution;
		this.printQuery = printQuery;
		
		

	}

	/**
	 * 
	 * @param name
	 * @param parent
	 * @return the generated attribute
	 * @throws PrologSyntaxException
	 * @throws OutOfBoundsException
	 */
	public PrologAttribute generatePrologAttribute(String name,
			PrologClass parent) throws PrologSyntaxException,
			OutOfBoundsException {
		AttributeConfiguration a = getAttributeConfigurationMap().get(name);
		if (a == null) {
			throw new NullPointerException(
					"No configuration data on a attribute with that name: "
							+ name);
		}

		return a.generatePrologAttribute(parent, this);
	}

	public boolean isPrintSolution() {
		return printSolution;
	}

	public void setPrintSolution(boolean printSolution) {
		this.printSolution = printSolution;
	}

	public boolean isPrintQuery() {
		return printQuery;
	}

	public void setPrintQuery(boolean printQuery) {
		this.printQuery = printQuery;
	}

	/**
	 * Helper method for retrieving the size of a class stored in the
	 * numberOfClasses map
	 * 
	 * @param className
	 * @return the size
	 * @throws OutOfBoundsException
	 * @throws NullPointerException
	 *             if the class name is not found in the map
	 */
	public Integer getClassSize(String className, int index)
			throws OutOfBoundsException {
		if (this.getNumberOfClassesMap().get(className) == null) {
			throw new NullPointerException(
					"No size stored for that class name (" + className + ")");
		} else if (this.getNumberOfClassesMap().get(className)[index] == null) {
			throw new OutOfBoundsException("No size for class:" + className
					+ "stored in this list at index: " + index);
		}
		return this.getNumberOfClassesMap().get(className)[index];
	}

	public String getQueryName() {
		return queryName;
	}

	/**
	 * @return the oclComplexity
	 */
	public int getOclComplexity() {
		return oclComplexity;
	}

	/**
	 * Helper method for retrieving the configuration value of an attribute
	 * stored in the attributeConfiguration map
	 * 
	 * @param attributeName
	 * @return a value between 0 and 1 (inclusive)
	 * @throws NullPointerException
	 *             if the attribute name is not found in the map
	 */
	public Double getAttributeConfigurationPercentageValue(String attributeName) {
		if (this.getAttributeConfigurationMap().get(attributeName) == null) {
			throw new NullPointerException(
					"No configuration stored for that attribute name ("
							+ attributeName + ")");
		}
		return this.getAttributeConfigurationMap().get(attributeName)
				.getPercentageConfigurated();
	}

	/**
	 * Contains mapping between class names, and how many objects of that class
	 * that we want to build
	 * 
	 * @return
	 */
	public Map<String, Integer[]> getNumberOfClassesMap() {
		return numberOfClassesMap;
	}

	// public int getNumberOfRuns() {
	// return numberOfRuns;
	// }

	/**
	 * @return the experimentResult
	 */
	public ArrayList<ExperimentResult> getExperimentResult() {
		return experimentResult;
	}

	/**
	 * @return the attributeConfigurationMap
	 */
	public Map<String, AttributeConfiguration> getAttributeConfigurationMap() {
		return attributeConfigurationMap;
	}

	/**
	 * @return the numOfClassIndex
	 */
	public int getNumOfClassIndex() {
		return numOfClassIndex;
	}

	/**
	 * @param numOfClassIndex
	 *            the numOfClassIndex to set
	 * @throws OutOfBoundsException
	 */
	public void setNumOfClassIndex(int numOfClassIndex)
			throws OutOfBoundsException {
		for (Integer[] i : getNumberOfClassesMap().values()) {
			if (i[numOfClassIndex] == null) {
				throw new OutOfBoundsException(
						"One (or more) of your lists of class sizes does not have an index this high. Failed first on this:"
								+ i);
			}
		}
		this.numOfClassIndex = numOfClassIndex;
	}

	/**
	 * @param numOfClassIndex
	 *            the numOfClassIndex to set
	 * @throws OutOfBoundsException
	 */
	public boolean incrementNumOfClassIndex() throws OutOfBoundsException {
		boolean canIncrement = true;
		
		if (getNumberOfClassesMap() != null) {
			for (Integer[] i : getNumberOfClassesMap().values()) {
				try {
					if (i[getNumOfClassIndex() + 1] == null) {
						canIncrement = false;
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					canIncrement = false;
				}

			}
			this.numOfClassIndex++;
		} 
		return canIncrement;
	}

}
