package rolfsnes.cp.prologexperiments.prologquerygenerator;

import java.util.Random;

import rolfsnes.cp.prologexperiments.exceptions.OutOfBoundsException;

public class Domain {

	private int min;
	private int max;
	private String list;
	private final boolean listInstance;

	/**
	 * Use this constructor for the regular integer attribute
	 * 
	 * @param min
	 * @param max
	 */
	public Domain(int min, int max) {
		this.setMin(min);
		this.setMax(max);
		this.listInstance = false;
	}

	/**
	 * Use this constructor for prolog lists (ex: [1,2,3,4] or [a,b,c,1,2,3]),
	 * where it does'nt make sense to talk about domain. Rather give an example
	 * instantiation.
	 * 
	 * @param list
	 */
	public Domain(String list) {
		this.setList(list);
		this.listInstance = true;
	}

	/**
	 * @return the min
	 */
	public int getMin() {
		return min;
	}

	/**
	 * @param min
	 *            the min to set
	 */
	public void setMin(int min) {
		this.min = min;
	}

	/**
	 * @return the max
	 */
	public int getMax() {
		return max;
	}

	/**
	 * @param max
	 *            the max to set
	 */
	public void setMax(int max) {
		this.max = max;
	}

	/**
	 * Gets a random value between min and max
	 * 
	 * @return
	 * @throws OutOfBoundsException
	 */
	public int getRandomInstantiation() throws OutOfBoundsException {
		int finalValue;
		if (getMin() != getMax()) {
			Random r = new Random();
			
			int firstRoll = r.nextInt(getMax() + 1);

			if (firstRoll <= getMin()) {
				finalValue = firstRoll + getMin();
			} else {
				finalValue = firstRoll;
			}

			if (finalValue < getMin() || finalValue > getMax()) {
				throw new OutOfBoundsException("The generated value: "
						+ finalValue + " is not in the range: " + getMin()
						+ " - " + getMax());
			}
		} else {
			finalValue = getMin();
		}
		return finalValue;
	}

	/**
	 * @return the list
	 */
	public String getList() {
		return list;
	}

	/**
	 * @param list
	 *            the list to set
	 */
	public void setList(String list) {
		this.list = list;
	}

	/**
	 * @return the listInstance
	 */
	public boolean isListInstance() {
		return listInstance;
	}
}
