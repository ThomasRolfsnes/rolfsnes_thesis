package rolfsnes.stat.Exceptions;

public class R_EvaluationError extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -297972245199972701L;

	public R_EvaluationError() {
	}

	public R_EvaluationError(String query) {
		super("Unable to evaluate:" +query);
	}
	
	

	public R_EvaluationError(Throwable cause) {
		super(cause);
	}

	public R_EvaluationError(String message, Throwable cause) {
		super(message, cause);
	}
}
