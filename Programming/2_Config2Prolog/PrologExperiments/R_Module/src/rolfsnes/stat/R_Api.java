package rolfsnes.stat;

import org.rosuda.JRI.REXP;
import org.rosuda.JRI.Rengine;

import rolfsnes.stat.Exceptions.R_EvaluationError;

public class R_Api {
	
	private Rengine re;
	private REXP x;
	
	
	/**
	 * 
	 * @param args - arguments to be passed to R. Please note that R requires the presence of certain arguments (e.g. --save or --no-save or equivalents), so passing an empty list usually doesn't work.
	 */
	public R_Api(String[] args){
		if (!Rengine.versionCheck()) {
		    System.err.println("** Version mismatch - Java files don't match library version.");
		    return;
		    //System.exit(1);
		}
		
		System.out.println("Creating Rengine (with arguments)");
		// 1) we pass the arguments from the command line
		// 2) we won't use the main loop at first, we'll start it later
		//    (that's the "false" as second argument)
		// 3) the callbacks are implemented by the TextConsole class above
		re=new Rengine(args, false, null);
		
		if (!re.waitForR()) {
            System.err.println("Cannot load R");
            return;
        }		
	}
	
	public String query(String rquery){
		return re.eval(rquery).toString();
	}
	
	public void readCSV(String fileLocation,String varName) throws R_EvaluationError{
		String query = varName+" <- read.csv(file=\"" + fileLocation + "\",header=TRUE,sep=\",\")";
		if(re.eval( query,false) == null){
			throw new R_EvaluationError(query);
		}
	}

	public void histogram(String var) throws R_EvaluationError{
		String query = "hist("+var+")";
		if(re.eval(query, false) == null){
			throw new R_EvaluationError(query);
		}
	}
	
	public void plot(String var1,String var2) throws R_EvaluationError{
		String query = "plot("+ var1 +"," + var2 +")";
		if(re.eval(query,false) == null){
			throw new R_EvaluationError(query);
		}
	}
	/**
	 * Use forward slash '/' as path seperator (even on windows). Use of backward slash will try to be converted.
	 * @param path
	 * @throws R_EvaluationError 
	 */
	public void setWorkingDirectory(String path) throws R_EvaluationError{
		String forwardSlashVersion = path.replaceAll("\\\\+", "/");
		String query = "setwd(\""+forwardSlashVersion+"\")";
		if(re.eval(query,false) == null){
			throw new R_EvaluationError(query);
		}
	}
	
	public void getWorkingDirectory() throws R_EvaluationError{
		String query = "getwd()";
		if(re.eval(query,false) == null){
			throw new R_EvaluationError(query);
		}
	}


	/**
	 * @return the x
	 */
	public REXP getX() {
		return x;
	}


	/**
	 * @param x the x to set
	 */
	public void setX(REXP x) {
		this.x = x;
	}
}
