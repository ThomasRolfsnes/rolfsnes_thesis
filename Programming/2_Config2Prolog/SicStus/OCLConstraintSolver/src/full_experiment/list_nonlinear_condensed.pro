/* -*- Mode:Prolog; coding:iso-8859-1; -*- */


:- use_module(library(clpfd)).
:- use_module(library(lists)).
:- use_module(library(aggregate)).
:- use_module(rolfsnes).


save:-
        save_program('list_nonlinear_condensed.sav').

%%%%%%%%%%%%%%%%%%%%%%%%
% CLASSIC IMPLEMENTATION %
%%%%%%%%%%%%%%%%%%%%%%%%

check_system(Model):-
        nth1(1,Model,ElectronicConnections),
        check_electronicConnections(ElectronicConnections,Model),!.
       

check_electronicConnections(ElectronicConnections,Model):-
        global_checks_electronicConnection(ElectronicConnections,Model).

global_checks_electronicConnection(ElectronicConnections,Model):-
        % non linear 1
        select_list([pinindex-3],#>,[ebindex-2],ElectronicConnections,Model,ElectronicConnections_sublist),
        collect_list([refs-4,sem-1],ElectronicConnections_sublist,Model,ElectronicConnections_SEMs),
        nth1(2,Model,SEMs),
        includes_all(ElectronicConnections_SEMs,SEMs),
      
        %non linear 2 (one navigation removed
        collect_list([refs-4,sem-1,eboards-2],ElectronicConnections,Model,ElectronicConnections_SEM_eboards),
        length0(ElectronicConnections_SEM_eboards,ElectronicConnections_SEM_eboards_size),
        collect_list([eboards-2],SEMs,Model,SEMs_eboards),
        length0(SEMs_eboards,SEMs_eboards_size),
        ElectronicConnections_SEM_eboards_size #>= SEMs_eboards_size,
        !.
 