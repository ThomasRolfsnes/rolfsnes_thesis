/* -*- Mode:Prolog; coding:iso-8859-1; -*- */
:- use_module(library(clpfd)).
:- use_module(library(lists)).
:- use_module(library(aggregate)).
:- use_module(library(assoc)).
:- use_module(rolfsnes).

save:-
        save_program('efficiency_comparison.sav').



check_system(List,ExecutionTime):-
        nested_list_to_assoc(List,Model),
        statistics(runtime, [T0|_]),
        get_assoc(electronicconnection,Model,EC),
        get_assoc(sem,Model,SEM),
        size_assoc(EC,ECSize),
        size_assoc(SEM,SEMSize),
        statistics(runtime, [T1|_]),
        ExecutionTime is T1 - T0.
        
        
        
        
        
a(A).
a(B).
% asd
entry(X,Y):-
        domain([X,Y], 0,10),
        Y #< X,
        next_goal(X,Y).
        

next_goal(X,Y):-
        X #= Y*2.
        