/* -*- Mode:Prolog; coding:iso-8859-1; -*- */


:- use_module(library(clpfd)).
:- use_module(library(lists)).
:- use_module(library(aggregate)).
:- use_module(library(assoc)).
:- use_module(rolfsnes).
save:-
        save_program('assoc_nonlinear_condensed.sav').


check_system(List):-
        nested_list_to_assoc(List,Model),
        get_assoc(electronicconnection,Model,ElectronicConnections),
        check_electronicConnections(ElectronicConnections,Model).

check_electronicConnections(ElectronicConnections,Model):-
       global_checks_electronicConnection(ElectronicConnections,Model).

global_checks_electronicConnection(ElectronicConnections,Model):-
        %non linear 1
        select_assoc([pinindex],#>,[ebindex],ElectronicConnections,Model,ElectronicConnections_subAssoc), %Li
        collect_assoc([refs,sem],ElectronicConnections_subAssoc,Model,ElectronicConnections_SEMs),
        get_assoc(sem,Model,SEMs),
        includes_all_assoc(ElectronicConnections_SEMs,SEMs),
        %non linear 2 (one navigation removed)
        collect_assoc([refs,sem,eboards],ElectronicConnections,Model,ElectronicConnections_SEM_eboards),
        size_assoc(ElectronicConnections_SEM_eboards,ElectronicConnections_SEM_eboards_size),       
        collect_assoc([eboards],SEMs,Model,SEMs_eboards),
        size_assoc(SEMs_eboards,SEMs_eboards_size),
        ElectronicConnections_SEM_eboards_size #>= SEMs_eboards_size,
        !.






