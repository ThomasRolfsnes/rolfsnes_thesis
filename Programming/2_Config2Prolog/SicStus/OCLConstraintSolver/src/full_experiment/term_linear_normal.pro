/* -*- Mode:Prolog; coding:iso-8859-1; -*- */

:- use_module(library(clpfd)).
:- use_module(library(lists)).
:- use_module(library(aggregate)).
:- use_module(rolfsnes).
save:-
        save_program('term_linear_normal.sav').

%%%%%%%%%%%%%%%%%%%%%%%%
% CLASSIC IMPLEMENTATION %
%%%%%%%%%%%%%%%%%%%%%%%%

check_system(Model):-
        arg(1,Model,ElectronicConnections),
        check_electronicConnections(ElectronicConnections,Model).
       

check_electronicConnections(ElectronicConnections,Model):-
        maplist(local_checks_electronicConnection(Model),ElectronicConnections),!.

local_checks_electronicConnection(Model,ElectronicConnection):-
        pin_range(ElectronicConnection,Model),
        board_in_range(ElectronicConnection,Model).
 


%Inv1: context ElectronicConnection inv PinRange
%pinIndex >= 0 and sem.eBoards->asSequence()->at(ebIndex+1).numOfPins > pinIndex
pin_range(ElectronicConnection,Model):-
        arg(2,ElectronicConnection,ebindex-EbIndex),
        arg(3,ElectronicConnection,pinindex-PinIndex),
        navigate_term([refs-4,sem-1,eboards-2],Model,ElectronicConnection,Eboards),
        PinIndex #>= 0,
        PinIndex #< 64, 
        domain_interval(Eboards,{8,16,32,64}),
        element0(EbIndex,Eboards,PinSize),
        PinSize #>= PinIndex,!.

%Inv2: context ElectronicConnection inv BoardIndRange 
%ebIndex >= 0 and ebIndex < sem.eBoards->size()
board_in_range(ElectronicConnection,Model):-
        arg(2,ElectronicConnection,ebindex-EbIndex),
        navigate_term([refs-4,sem-1,eboards-2],Model,ElectronicConnection,Eboards),
        EbIndex #> 0,
        length0(Eboards,EboardsSize),
        EbIndex #=< EboardsSize,!.









