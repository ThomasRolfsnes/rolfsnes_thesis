/* -*- Mode:Prolog; coding:iso-8859-1; -*- */



term_includes_all(Part,Whole):-
        (foreacharg(Arg,Part,Index),param(Whole) do
         arg(Index,Whole,Arg)).


% Augments built in sort/2 with the ability to handle input List as variable.
% In that case, just returns the same variable. Otherwise, use sort/2
v_sort(List,List):-
        var(List),
        !.
        
v_sort(List,Sorted):-
        sort(List,Sorted).



prime(Max,_,Max).
prime(Current,ListOfPrimes,Max):-
        (checkOne(Current) -> append(Found,Current,ListOfPrimes)),
        CheckNext is Current + 1,
        !,
        prime(CheckNext,Found,Max).
        

checkOne(Number):-
        PreviousNumber is Number -1,
        checkOne(Number,PreviousNumber).

checkOne(_,0).
checkOne(Number,Check):-
        0 is (Number mod Check).
        
        
checkOne(Number,Check):-
        1 is (Number mod Check),
        CheckNext is Check - 1,
        checkOne(Number,CheckNext).
                