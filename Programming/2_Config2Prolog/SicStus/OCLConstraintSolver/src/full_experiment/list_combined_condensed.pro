/* -*- Mode:Prolog; coding:iso-8859-1; -*- */

:- use_module(library(clpfd)).
:- use_module(library(lists)).
:- use_module(library(aggregate)).
:- use_module(rolfsnes).

save:-
        save_program('list_combined_condensed.sav').

check_system(Model):-
        nth1(1,Model,ElectronicConnections),
        check_electronicConnections(ElectronicConnections,Model),!.
       

check_electronicConnections(ElectronicConnections,Model):-
        maplist(local_checks_electronicConnection(Model),ElectronicConnections),
        global_checks_electronicConnection(ElectronicConnections,Model),!.

local_checks_electronicConnection(Model,ElectronicConnection):-
        % PIN RANGE
        nth1(2,ElectronicConnection,ebindex-EbIndex),
        nth1(3,ElectronicConnection,pinindex-PinIndex),
        navigate_list([refs-4,sem-1,eboards-2],Model,ElectronicConnection,Eboards),     
        PinIndex #>= 0,
        PinIndex #< 64, 
        domain_interval(Eboards,{8,16,32,64}),
        element0(EbIndex,Eboards,PinSize),
        PinSize #>= PinIndex,
        
        %board in range (two navigations removed)
        EbIndex #> 0,
        length0(Eboards,EboardsSize),
        EbIndex #=< EboardsSize,!.


global_checks_electronicConnection(ElectronicConnections,Model):-
       % non linear 1
        select_list([pinindex-3],#>,[ebindex-2],ElectronicConnections,Model,ElectronicConnections_sublist),
        collect_list([refs-4,sem-1],ElectronicConnections_sublist,Model,ElectronicConnections_SEMs),
        nth1(2,Model,SEMs),
        includes_all(ElectronicConnections_SEMs,SEMs),
      
        %non linear 2 (one navigation removed
        collect_list([refs-4,sem-1,eboards-2],ElectronicConnections,Model,ElectronicConnections_SEM_eboards),
        length0(ElectronicConnections_SEM_eboards,ElectronicConnections_SEM_eboards_size),
        collect_list([eboards-2],SEMs,Model,SEMs_eboards),
        length0(SEMs_eboards,SEMs_eboards_size),
        ElectronicConnections_SEM_eboards_size #>= SEMs_eboards_size,
        !.
        

 