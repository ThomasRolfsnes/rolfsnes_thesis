/* -*- Mode:Prolog; coding:iso-8859-1; -*- */

:- use_module(library(clpfd)).
:- use_module(library(lists)).
:- use_module(library(aggregate)).
:- use_module(library(assoc)).
:- use_module(rolfsnes).
save:-
        save_program('assoc_linear_condensed.sav').

check_system(List):-
        nested_list_to_assoc(List,Model),
        get_assoc(electronicconnection,Model,ElectronicConnections),
        check_electronicConnections(ElectronicConnections,Model).

check_electronicConnections(ElectronicConnections,Model):-
       map_assoc(local_checks_electronicConnection(Model),ElectronicConnections),!.

local_checks_electronicConnection(Model,ElectronicConnection):-
        %pin range
        get_assoc(ebindex,ElectronicConnection,EbIndex),
        get_assoc(pinindex,ElectronicConnection,PinIndex),
        navigate_assoc([refs,sem,eboards],Model,ElectronicConnection,Eboards),
        PinIndex #>= 0,
        PinIndex #< 64, 
        domain_interval(Eboards,{8,16,32,64}),
        element0(EbIndex,Eboards,PinSize),
        PinSize #>= PinIndex,
        %board in range (two navigations removed
        EbIndex #> 0,
        length0(Eboards,EboardsSize),
        EbIndex #=< EboardsSize,!.

