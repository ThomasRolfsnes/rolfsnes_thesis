/* -*- Mode:Prolog; coding:iso-8859-1; -*- */


:- module(rolfsnes, [
        navigate_assoc/4,               %Key x ModelAssoc x Assoc -> Assoc
        navigate_assoc0/4,              %Key x ModelAssoc x Assoc -> Key-Assoc
        navigate_term/4,
        navigate_list/4,
        %navigate_hybrid_assoc/4,        %Key x ModelAssoc x Assoc -> Assoc
        nested_list_to_hybrid_assoc/2,  %List -> HybridAssoc                 
        nested_list_to_assoc/2,         %List -> Assoc
        collect_assoc/4,                %NavigationList x Assoc x ModelAssoc -> NewAssoc
        collect_term/4,
        collect_list/4,
        select_assoc/6,                 %NavigationList x Rel x Value x Assoc x ModelAssoc -> NewAssoc
        select_term/6,
        select_list/6,
        includes_all_assoc/2,           %WholeAssoc x PartAssoc ->
        includes_all/2,
        map_assoc_to_list/3,            %Pred x Assoc -> List
        size_assoc/2,
        check_attribute_assoc/5,        %Rel x Value x NavigationList x AssocModel x Assoc
        check_attribute_term/5,
        check_attribute_list/5,
        element0/3,                     %Integer x List -> Element
        length0/2,                      %List -> Integer
        domain_interval/2,              %List x Interval
        is_pair_list/1                  %List ->
                    
                    
                    
                    
                    
                    
                    ]).


:- use_module(library(clpfd)).
:- use_module(library(lists)).
:- use_module(library(aggregate)).
:- use_module(library(assoc)).



/*
PROLOG MODEL GENERATION
*/

nested_list_to_hybrid_assoc(List,Assoc):-
        nested_list_to_hybrid_assoc(List, Assoc,0). 


nested_list_to_hybrid_assoc(List, Assoc,Depth) :-
        (is_pair_list(List) -> 
        keysort(List, Pairs),
        length(Pairs, N),
        nested_list_to_hybrid_assoc(N, Pairs, [], Assoc,Depth)
        ;
        Assoc = List).

nested_list_to_hybrid_assoc(0, List, List, assoc,_) :- !.
nested_list_to_hybrid_assoc(N, List, Rest, assoc(Key,Val2,L,R),0) :-
        A is (N-1) >> 1,
        Z is (N-1) - A,
        nested_list_to_hybrid_assoc(A, List, [Key-Val|More], L,0),
        (nonvar(Val) -> nested_list_to_hybrid_assoc(Val,Val2,1) ; Val2 = Val),
        nested_list_to_hybrid_assoc(Z, More, Rest, R,0).
        

nested_list_to_hybrid_assoc(N, List, Rest, ListForm-assoc(Key,Val2,L,R),1) :-
        A is (N-1) >> 1,
        Z is (N-1) - A,
        nested_list_to_hybrid_assoc(A, List, [Key-Val|More], L,2),
        (nonvar(Val) -> nested_list_to_hybrid_assoc(Val,Val2,2) ; Val2 = Val),
        nested_list_to_hybrid_assoc(Z, More, Rest, R,2),
        assoc_to_list(assoc(Key,Val2,L,R),ListForm).

nested_list_to_hybrid_assoc(N, List, Rest, assoc(Key,Val2,L,R),2) :-
        A is (N-1) >> 1,
        Z is (N-1) - A,
        nested_list_to_hybrid_assoc(A, List, [Key-Val|More], L,2),
        (nonvar(Val) -> nested_list_to_hybrid_assoc(Val,Val2,2) ; Val2 = Val),
        nested_list_to_hybrid_assoc(Z, More, Rest, R,2).

%@ Adapted from code written by Richard A. O'Keefe, from the SiCStus assoc library
%@  @item list_to_assoc(@var{+List}, @var{-Assoc})
%@  @PLXindex {list_to_assoc/2 (assoc)}
%@  is true when @var{List} is a proper list of @var{Key-Val} pairs (in any order)
%@  and @var{Assoc} is an association tree specifying the same finite function
%@  from @var{Keys} to @var{Values}.  Note that the list should not contain any
%@  duplicate keys.  In this release, @code{list_to_assoc/2} doesn't check for
%@  duplicate keys, but the association tree which gets built won't work.
%@ 
%@ This version has been augmented to build association lists from lists of key-value pairs
%@ nested within the value of other key-value pairs. So we will get a tree where the nodes
%@ also can be trees.
nested_list_to_assoc(List, Assoc) :-
        (is_pair_list(List) -> 
        keysort(List, Pairs),
        length(Pairs, N),
        nested_list_to_assoc(N, Pairs, [], Assoc)
        ;
        Assoc = List).

nested_list_to_assoc(0, List, List, assoc) :- !.
nested_list_to_assoc(N, List, Rest, assoc(Key,Val2,L,R)) :-
        A is (N-1) >> 1,
        Z is (N-1) - A,
        nested_list_to_assoc(A, List, [Key-Val|More], L),
        (nonvar(Val) -> nested_list_to_assoc(Val,Val2) ; Val2 = Val),
        nested_list_to_assoc(Z, More, Rest, R).
/*
NAVIGATION 
*/

%@ @item navigate_assoc(@var{KeyList},@var{Model},@var{Assoc},@var{Result})
%@
%@ Navigates @var{Assoc} according to the list of navigation steps given in @var{KeyList}.
%@ Just returns the value of the found node
navigate_assoc([Key],_,Assoc,Element):-
        get_assoc(Key,Assoc,Element).

navigate_assoc([refs,ClassKey|Rest],Model,Assoc,Result):-
        get_assoc(refs,Assoc,Refs),
        get_assoc(ClassKey,Refs,Object),
        (is_assoc(Object) -> 
                (is_multiplicity_one_assoc(Object) -> get_first_key_value_assoc(Object,_,Element))
                ;
                get_assoc(ClassKey,Model,ClassList),
                get_assoc(Object,ClassList,Element)),
        (Rest == [] -> Result = Element ; navigate_assoc(Rest,Model,Element,Result)).
        
navigate_assoc([Key|Rest],Model,Assoc,Result):-
        get_assoc(Key,Assoc,Element),
        navigate_assoc(Rest,Model,Element,Result).

%@ @item navigate_assoc(@var{KeyList},@var{Model},@var{Assoc},@var{Result})
%@
%@ Navigates @var{Assoc} according to the list of navigation steps given in @var{KeyList}.
%@ This version return the found node as a Key-Value pair.
navigate_assoc0([Key],_,Assoc,Key-Element):-
        get_assoc(Key,Assoc,Element).

navigate_assoc0([refs,ClassKey|Rest],Model,Assoc,Result):-
        get_assoc(refs,Assoc,Refs),
        get_assoc(ClassKey,Refs,Object),
        (is_assoc(Object) -> 
                (is_multiplicity_one_assoc(Object) -> get_first_key_value_assoc(Object,Key,Element))
                ;
                get_assoc(ClassKey,Model,ClassList),
                get_assoc(Object,ClassList,Element),
                Key = Object),
        (Rest == [] -> Result = Key-Element ; navigate_assoc0(Rest,Model,Element,Result)).
        
navigate_assoc0([Key|Rest],Model,Assoc,Result):-
        get_assoc(Key,Assoc,Element),
        navigate_assoc0(Rest,Model,Element,Result).


%@ @item navigate_term(@var{Type-Index List},@var{Model},@var{Term},@var{Result})
navigate_term([_-Index],_,List,Result):-
        arg(Index,List,_-Result).

navigate_term([refs-Index,_-ClassKeyIndex|Rest],Model,List,Result):-
        arg(Index,List,_-Refs),
        nth1(ClassKeyIndex,Refs,Key-Object),
        (Key-Object = _-_-_ -> resolveReference_term(Key-Object,Model,Element), 
                                 (Rest == [] -> Result = Element 
                                                ;
                                                navigate_term(Rest,Model,Element,Result))
                                 ; 
                                 (Rest == [] -> Object = Result
                                               ;
                                                navigate_term(Rest,Model,Object,Result))).
        
navigate_term([_-Index|NextIndex],Model,List,Result):-
        arg(Index,List,_-Element),
        navigate_term(NextIndex,Model,Element,Result).

resolveReference_term(_-ClassID-ObjectID,Model,Object):-
        arg(ClassID,Model,ClassList),
        nth1(ObjectID,ClassList,Object),
        !.

%@ @item navigate_term(@var{Type-Index List},@var{Model},@var{Term},@var{Result})
navigate_list([Type-Index],_,List,Result):-
        nth1(Index,List,Type-Result).

navigate_list([refs-Index,_-ClassKeyIndex|Rest],Model,List,Result):-
        nth1(Index,List,_-Refs),
        nth1(ClassKeyIndex,Refs,Key-Object),
        (Key-Object = _-_-_ -> resolveReference_list(Key-Object,Model,Element), 
                                 (Rest == [] -> Result = Element 
                                                ;
                                                navigate_list(Rest,Model,Element,Result))
                                 ; 
                                 (Rest == [] -> Object = Result
                                               ;
                                                navigate_list(Rest,Model,Object,Result))).
        
navigate_list([_-Index|NextIndex],Model,List,Result):-
        nth1(Index,List,_-Element),
        navigate_list(NextIndex,Model,Element,Result).

resolveReference_list(_-ClassID-ObjectID,Model,Object):-
        nth1(ClassID,Model,ClassList),
        nth1(ObjectID,ClassList,Object),
        !.



%%@ @item navigate_hybrid_assoc(@var{KeyList},@var{HybridModel},@var{Assoc},@var{Result})
%navigate_hybrid_assoc([],_,Assoc,Assoc):-!.
%
%navigate_hybrid_assoc([Key],_,Assoc,Element):-
%        get_assoc(Key,Assoc,Element),!.
%
%navigate_hybrid_assoc([refs,ClassKey|Rest],Model,Assoc,Result):-
%        get_assoc(refs,Assoc,Refs),
%        get_assoc(ClassKey,Refs,ObjectKey),
%        get_assoc(ClassKey,Model,ClassList),
%        get_assoc(ObjectKey,ClassList,Element),
%        navigate_hybrid_assoc(Rest,Model,Element,Result).
%
%navigate_hybrid_assoc([Key|Rest],Model,Assoc,Result):-
%        get_assoc(Key,Assoc,Element),
%        navigate_hybrid_assoc(Rest,Model,Element,Result).


/*
SELECT
*/

%@ @item select_assoc(@var{NavLeft},@var{Rel},@var{NavRight},@var{Assoc},@var{Model},@var{-NewAssoc})
%@
%@ Iterates over @var{Assoc}, doing the navigation steps in @var{Nav} on each node, and checks the found element as 'Found Rel Value'
%@ If true, adds the orginating node to output tree.
select_assoc(NavLeft,Rel,NavRight,Assoc,Model,NewAssoc):-
        select_assoc(NavLeft,Rel,NavRight,Model,Assoc,List,[]),
        list_to_assoc(List,NewAssoc).

select_assoc(Key-Value) --> [Key-Value].
select_assoc([]) --> [].
select_assoc(_,_,_,_,assoc) --> [],{!}.

select_assoc(NavLeft,Rel,NavRight,Model,assoc(Key,Value,L,R)) -->
        select_assoc(NavLeft,Rel,NavRight,Model,L),
        select_assoc(Out),
        select_assoc(NavLeft,Rel,NavRight,Model,R),
        {check_attribute_assoc(NavLeft,Rel,NavRight,Model,Value) -> Out = Key-Value ; Out = []}.


select_term(NavLeft,Rel,NavRight,Term,Model,NewList):-
       include(check_attribute_term(NavLeft,Rel,NavRight,Model),Term,NewList).


select_list(NavLeft,Rel,NavRight,List,Model,NewList):-
       include(check_attribute_list(NavLeft,Rel,NavRight,Model),List,NewList).



/*
COLLECT
*/

%@ @item collect_assoc(@var{Nav},@var{Assoc},@var{Model},@var{NewAssoc})
%@
%@ Collects the elements from Assoc resulting from doing the navigation steps in @var{Nav}
%@ on each node of @var{Assoc}, and builds a new tree from those elements. O(N log(N))
collect_assoc(Nav,Assoc,Model,NewAssoc):-
        map_assoc_to_list(navigate_assoc0(Nav,Model),Assoc,Result),
        list_to_assoc(Result,NewAssoc).

%@ @item collect_term(@var{NavigationList},@var{Term},@var{Model},@var{Result})
collect_term(NavigationList,Term,Model,Result):-
        maplist(navigate_term(NavigationList,Model),Term,Result),!.

collect_list(NavigationsList,List,Model,Result):-
        maplist(navigate_list(NavigationsList,Model),List,Result),!.
      
/*
INCLUDES ALL
*/       

%@ @item includes_all_assoc(@var{WholeAssoc},@var{PartAssoc})
%@
%@ Checks if all Key-Value pairs in @var{PartAssoc} exists in @var{WholeAssoc}
%@ O(M log(N)), where M is the size of @var{PartAssoc} and N is the size of @var{WholeAssoc}
includes_all_assoc(_,assoc):-!.
includes_all_assoc(WholeAssoc,assoc(Key,Value,L,R)):-
        (get_assoc(Key,WholeAssoc,Value2),Value #= Value2 ->
                includes_all_assoc(WholeAssoc,L),
                includes_all_assoc(WholeAssoc,R)).
               

%@ @item includes_all_list(@var{Whole},@var{Part})
%@
%@
%@ Checks if the list 'Part' is a sublist of the list 'Whole'
%@ The lists may be equal.
includes_all(_,[]):-!.
includes_all(Whole,Part):-
        sort(Part,PartSorted),
        sort(Whole,WholeSorted),
        subseq0(WholeSorted,PartSorted),!.
/*
ITERATORS
*/

%@ item map_assoc_to_list(@var{+Pred},@var{+Assoc},@var{-List})
%@
%@ Iterates through an assoc tree applying Pred on the value of each node. The result is appended to @var{List}
map_assoc_to_list(Pred,Assoc,List):-
        map_assoc_to_list(Pred,Assoc,List,[]).

map_assoc_to_list(_,assoc) --> [],{!}.
map_assoc_to_list(Pred,assoc(_,Val,L,R)) -->
        map_assoc_to_list(Pred,L),
        [Out],
        map_assoc_to_list(Pred,R),
        {call(Pred,Val,Out),!}.


/*
UTILITY
*/

%@ @item
size_assoc(assoc,0).
size_assoc(assoc(_,_,L,R),Size):-
        size_assoc(L,LeftSize),
        size_assoc(R,RightSize),
        Size is LeftSize + RightSize + 1.

%@ @item constrain_attribute(@var{+NavLeft},@var{Rel},@var{+NavRight},@var{+Model},@var{+Assoc})
%@ @PLXindex {constrain_attribute/5 (ocl_assoc)}
%@ Constrains found attribute (using list of navigations @var{NavLeft}) using the given @var{Rel} relation
%@ and comparing with navigation @var{NavRight}. Alternatively, an integer can take the place of any navigation.
%@ Expects @var{Assoc} and @var{Model} to be proper assoc trees
check_attribute_assoc(NavLeft,Rel,NavRight,Model,AttributeAssoc):-
        (integer(NavRight) -> ValueRight = NavRight ; navigate_assoc(NavRight,Model,AttributeAssoc,ValueRight)), 
        (integer(NavLeft) -> ValueLeft = NavLeft ; navigate_assoc(NavLeft,Model,AttributeAssoc,ValueLeft)), 
        call(Rel,ValueLeft,ValueRight),!.

check_attribute_term(NavLeft,Rel,NavRight,Model,Term):-
        (integer(NavRight) -> ValueRight = NavRight ; navigate_term(NavRight,Model,Term,ValueRight)), 
        (integer(NavLeft) -> ValueLeft = NavLeft ; navigate_term(NavLeft,Model,Term,ValueLeft)),
        call(Rel,ValueLeft,ValueRight),!.

check_attribute_list(NavLeft,Rel,NavRight,Model,List):-
        (integer(NavRight) -> ValueRight = NavRight ; navigate_list(NavRight,Model,List,ValueRight)), 
        (integer(NavLeft) -> ValueLeft = NavLeft ; navigate_list(NavLeft,Model,List,ValueLeft)),
        call(Rel,ValueLeft,ValueRight),!.



%@ @item is_pair_list(@var{List})
%@
%@ Checks if @var{List} is a list of key value pairs where key is ground
is_pair_list([Key-_]):-
        nonvar(Key),!.
is_pair_list([Key-_|Next]):-
        (nonvar(Key) -> is_pair_list(Next)).

%@ @item length0(@var{List},@var{Size})
%@
%@
%@ Augments built in length/2 in the following way:
%@ Simply calls the length/2 predicate, except when List and Size is a variable. In that case,
%@ size is given the domain 0..sup.
length0(List,Size):-
        (var(List),
        var(Size),
                Size in 0..sup
                ,!;
                length(List,Size)).



%@ @item element0(@var(Index),@var{List},@var{Element})
%@
%@
%@ Augments clpfd:element/3 in the following way: 
%@ Simple calls the element/3 predicate, except when List is a variable. In that case,
%@ @var{Index} is given the domain 0..sup, and @var{Element} is given the domain inf..sup
element0(Index,List,Element):-
         (var(List) ->
                Index in 1..sup, %then
                Element in inf..sup, %--||--
                !;
                element(Index,List,Element)). %else



%@ @item domain_interval(@var{List},@var{Interval})
%@
%@
%@ Assign domain to a list of variables, where Interval is on the form: {0,56,8} etc.
domain_interval(List,Interval):-
        (nonvar(List) -> maplist(check_domain(Interval),List),!
                         ;
                         !).

check_domain(Interval,Element):-
        Element in Interval,!.


is_multiplicity_one_assoc(assoc(_,_,assoc,assoc)).
        
get_first_key_value_assoc(assoc(Key,Value,_,_),Key,Value).

%%%%%%%%%%%%%%%%%%
% Error handling %
%%%%%%%%%%%%%%%%%%

%Type check for references (ref-X-X)
check_type(Type1,Type2-_):-
        (Type1 \== Type2,
                throw(mismatching_types(Type1,Type2));
         !).
%Type check for attributes (Type-Value)
check_type(Type1,Type2):-
        (Type1 \== Type2,
                throw(mismatching_types(Type1,Type2));
         true).



