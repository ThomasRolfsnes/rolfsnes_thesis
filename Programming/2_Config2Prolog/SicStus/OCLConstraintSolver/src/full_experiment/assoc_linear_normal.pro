/* -*- Mode:Prolog; coding:iso-8859-1; -*- */


:- use_module(library(clpfd)).
:- use_module(library(lists)).
:- use_module(library(aggregate)).
:- use_module(library(assoc)).
:- use_module(rolfsnes).
save:-
        save_program('assoc_linear_normal.sav').


check_system(List):-
        nested_list_to_assoc(List,Model),
        get_assoc(electronicconnection,Model,ElectronicConnections),
        check_electronicConnections(ElectronicConnections,Model).

check_electronicConnections(ElectronicConnections,Model):-
       map_assoc(local_checks_electronicConnection(Model),ElectronicConnections),!.
      
local_checks_electronicConnection(Model,ElectronicConnection):-
        pin_range(ElectronicConnection,Model),
        board_in_range(ElectronicConnection,Model).
 

%Inv1: context ElectronicConnection inv PinRange
%pinIndex >= 0 and sem.eBoards->asSequence()->at(ebIndex+1).numOfPins > pinIndex
pin_range(ElectronicConnection,Model):-
        get_assoc(ebindex,ElectronicConnection,EbIndex),
        get_assoc(pinindex,ElectronicConnection,PinIndex),
        navigate_assoc([refs,sem,eboards],Model,ElectronicConnection,Eboards),
        PinIndex #>= 0,
        PinIndex #< 64, 
        domain_interval(Eboards,{8,16,32,64}),
        element0(EbIndex,Eboards,PinSize),
        PinSize #>= PinIndex,!.

%Inv2: context ElectronicConnection inv BoardIndRange 
%ebIndex >= 0 and ebIndex < sem.eBoards->size()
board_in_range(ElectronicConnection,Model):-
        get_assoc(ebindex,ElectronicConnection,EbIndex),
        navigate_assoc([refs,sem,eboards],Model,ElectronicConnection,Eboards),
        EbIndex #> 0,
        %PinSize #> 0,
        length0(Eboards,EboardsSize),
        EbIndex #=< EboardsSize,!.



