/* -*- Mode:Prolog; coding:iso-8859-1; -*- */

:- use_module(library(clpfd)).
:- use_module(library(lists)).
:- use_module(library(aggregate)).
:- use_module(rolfsnes).

save:-
        save_program('list_combined_normal.sav').

check_system(Model):-
        nth1(1,Model,ElectronicConnections),
        check_electronicConnections(ElectronicConnections,Model),!.
       

check_electronicConnections(ElectronicConnections,Model):-
        maplist(local_checks_electronicConnection(Model),ElectronicConnections),
        global_checks_electronicConnection(ElectronicConnections,Model),!.

local_checks_electronicConnection(Model,ElectronicConnection):-
        pin_range(ElectronicConnection,Model),
        board_in_range(ElectronicConnection,Model).


global_checks_electronicConnection(ElectronicConnections,Model):-
        non_linear_1(ElectronicConnections,Model),
        non_linear_2(ElectronicConnections,Model).
        

 

% example:
% non_linear_1([[ec_0,Eindex_1,Pindex_1,[sem_0,Eboards_1,Size_1]]],[[sem_0,Eboards_1,Size_1]]).
non_linear_1(ElectronicConnections,Model):-
        
        select_list([pinindex-3],#>,[ebindex-2],ElectronicConnections,Model,ElectronicConnections_sublist),
        collect_list([refs-4,sem-1],ElectronicConnections_sublist,Model,ElectronicConnections_SEMs),
        nth1(2,Model,SEMs),
        includes_all(ElectronicConnections_SEMs,SEMs),
        !.
 
%"ElectronicConnection.allInstances()->collect(c|c.sem.eBoards)->size() > SEM.allInstances()->collect(c|c.eBoards)->size()", //quadratic 2

non_linear_2(ElectronicConnections,Model):-
        collect_list([refs-4,sem-1,eboards-2],ElectronicConnections,Model,ElectronicConnections_SEM_eboards),
        length0(ElectronicConnections_SEM_eboards,ElectronicConnections_SEM_eboards_size),
        nth1(2,Model,SEMs),
        collect_list([eboards-2],SEMs,Model,SEMs_eboards),
        length0(SEMs_eboards,SEMs_eboards_size),
        ElectronicConnections_SEM_eboards_size #>= SEMs_eboards_size,
        !.
        
        




        


%Inv1: context ElectronicConnection inv PinRange
%pinIndex >= 0 and sem.eBoards->asSequence()->at(ebIndex+1).numOfPins > pinIndex
pin_range(ElectronicConnection,Model):-
        nth1(2,ElectronicConnection,ebindex-EbIndex),
        nth1(3,ElectronicConnection,pinindex-PinIndex),
        navigate_list([refs-4,sem-1,eboards-2],Model,ElectronicConnection,Eboards),     
        PinIndex #>= 0,
        PinIndex #< 64, 
        domain_interval(Eboards,{8,16,32,64}),
       % v_sort(Eboards,EboardsSorted),
        element0(EbIndex,Eboards,PinSize),
        PinSize #>= PinIndex,!.

%Inv2: context ElectronicConnection inv BoardIndRange 
%ebIndex >= 0 and ebIndex < sem.eBoards->size()
board_in_range(ElectronicConnection,Model):-
        nth1(2,ElectronicConnection,ebindex-EbIndex),
        navigate_list([refs-4,sem-1,eboards-2],Model,ElectronicConnection,Eboards),
        EbIndex #> 0,
        %PinSize #> 0,
        length0(Eboards,EboardsSize),
        EbIndex #=< EboardsSize,!.

