% Author: Razieh Behjati
%	  Simula Research Laboratory
% Date:   10.Jan.2012
% For usage see examples at the end of this file.
%
% Change from SemExampleV9.pl:
%
% Fixed calculation of valid ranges for EBoard items, in cases where the item has already got a value.
% This modification should be added to V8, since it is not an optimization and is required for the tool 
% to function correctly.
% 
% Also I have simplified the calculation of NumOfPins. The "enumId('ElectronicBoard', ..., ...)" clauses
% are removed.
%


:- use_module(library(clpfd)).
:- use_module(library(lists)).

class('SubseaField').
class('XmasTree').
class('SCM').
class('SEM').
class('Device').
class('Connection').

classId('SubseaField', 0).
classId('XmasTree', 1).
classId('SCM', 2).
classId('SEM', 3).
classId('Device', 4).
classId('Connection', 5).

propId('SubseaField', 'id', 0).
propId('SubseaField', 'xts', 1).

propId('XmasTree', 'id', 0).
propId('XmasTree', 'SCM', 1).
propId('XmasTree', 'devices', 2).

propId('SCM', 'id', 0).
propId('SCM', 'type', 1).
propId('SCM', 'sems', 2).

propId('SEM', 'id', 0).
propId('SEM', 'eBoards', 1).
propId('SEM', 'connections', 2).
propId('SEM', 'devices', 3).

propId('Connection', 'id', 0).
propId('Connection', 'pinInd', 1).
propId('Connection', 'eBoardInd', 2).
propId('Connection', 'device', 3).
propId('Connection', 'sem', 4).

propId('Device', 'id', 0).

enum('CMType').
enum('ElectronicBoard').

enumId('CMType', 'PRODUCTION', 0).
enumId('CMType', 'INJECTION', 1).

numOfPins(0, 64).
numOfPins(1, 32).
numOfPins(2, 16).
numOfPins(3, 8).

builtin('int').
builtin('boolean').
builtin('string').

% check_*** checks all OCL constraints where the context is ***.

check_system([], []).
check_system(AI, Ids) :-
	check_subseaFields(AI, Ids), 
	check_connections(AI, Ids).
	
check_subseaFields(AI, Ids) :-
	get_instance_ids('SubseaField', Ids, FieldIds),
	get_objects(AI, FieldIds, Fields),
	all_xts_nums_in_range(AI, Ids, Fields).

all_xts_nums_in_range(_, _, []).
all_xts_nums_in_range(AI, Ids, [Field0| Rest]) :-
	one_xts_nums_in_range(AI, Ids, Field0),
	all_xts_nums_in_range(AI, Ids, Rest).

one_xts_nums_in_range(_, _, [[0], _, _, Xts, _, [XtsLen]]) :-
	length(Xts, L),
        L #> 0,
        XtsLen #>= L.

check_connections(AI, Ids) :-
	get_instance_ids('Connection', Ids, ConIds),
	get_objects(AI, ConIds, Conns),
	all_e_board_ind_in_range(AI, Ids, Conns),
	all_pin_range(AI, Ids, Conns).

all_e_board_ind_in_range(_, _, []).
all_e_board_ind_in_range(AI, Ids, [Obj0|Rest]) :-
	one_e_board_in_range(AI, Ids, Obj0),
	all_e_board_ind_in_range(AI, Ids, Rest).

one_e_board_in_range(AI, _, [[5],[_],[OwnerId],_, _, [BoardInd], [BoardIndVar], _, _]) :-
	BoardInd #>= 0,
	BoardIndVar #>= 0,
	get_object(AI, OwnerId, [_,_,_,EBoards, _, _, _, _, _]),
	length(EBoards, L),
	BoardInd #< L,
	BoardIndVar #< L.

all_pin_range(_,_,[]).
all_pin_range(AI, Ids, [Obj0|Rest]) :-
    one_pin_range(AI, Ids, Obj0),
    all_pin_range(AI, Ids, Rest).
    

one_pin_range(AI, _, [[5],[_],[OwnerId],[PinInd], [_], [BoardInd], _, _, _]) :-
    var(PinInd),
    var(BoardInd),
    get_object(AI, OwnerId, [_,_,_,EBoards, _, _, _, _, _]),
    has_var(EBoards), 
    PinInd in 0..63.
    
one_pin_range(AI, _, [[5],[_],[OwnerId],[PinInd], [_], [BoardInd], _, _, _]) :-
	var(PinInd),
    var(BoardInd),  
    get_object(AI, OwnerId, [_,_,_,EBoards, _, _, _, _, _]),
    no_var(EBoards),
    min_member(Max, EBoards), % 64_PIN is mapped to 0, 32_PIN to 1, and so on (I guess, comment added on Jan 15th, 2013)
    numOfPins(Max, NumOfPins),
    PinInd #>= 0,
	NumOfPins #> PinInd.
	
one_pin_range(AI, _, [[5],[_],[OwnerId],[PinInd], [_], [BoardInd], _, _, _]) :-
	var(PinInd),
    number(BoardInd),  
    get_object(AI, OwnerId, [_,_,_,EBoards, _, _, _, _, _]),
    nth0(BoardInd, EBoards, BoardId),
    var(BoardId),
    PinInd in 0..63. 
	
one_pin_range(AI, _, [[5],[_],[OwnerId],[PinInd], [_], [BoardInd], _, _, _]) :-
	var(PinInd),
    number(BoardInd), 
    get_object(AI, OwnerId, [_,_,_,EBoards, _, _, _, _, _]),
    nth0(BoardInd, EBoards, BoardId),
    number(BoardId),
	numOfPins(BoardId, NumOfPins),
	PinInd #>= 0,
	NumOfPins #> PinInd.
   
one_pin_range(AI, _, [[5],[_],[OwnerId],[PinInd], [PinIndVar], [BoardInd], _, _, _]) :-
    number(PinInd),
    var(BoardInd),
    PinInd #>= 0,
    PinIndVar #>= 0,
    get_object(AI, OwnerId, [_,_,_,EBoards, EBVar, _, _, _, _]),
	nth0(BoardInd, EBoards, BoardId),
	numOfPins(BoardId, NumOfPins),
	NumOfPins #> PinInd,
	NumOfPins #> PinIndVar,
	nth0(BoardInd, EBVar, BIdVar),
	numOfPins(BIdVar, NPVar),
	NPVar #> PinInd.
	
one_pin_range(AI, _, [[5],[_],[OwnerId],[PinInd], [PinIndVar], [BoardInd], [BoardIndVar], _, _]) :-
    number(PinInd),
    number(BoardInd),
    PinInd #>= 0,
    PinIndVar #>= 0,
    get_object(AI, OwnerId, [_,_,_,EBoards, EBVar, _, _, _, _]),
	nth0(BoardInd, EBoards, BoardId),
	numOfPins(BoardId, NumOfPins),
	NumOfPins #> PinInd,
	NumOfPins #> PinIndVar,
	nth0(BoardIndVar, EBoards, BoardIdVar),
	numOfPins(BoardIdVar, NumOfPinsVar),
	NumOfPinsVar #> PinInd,
	nth0(BoardInd, EBVar, BIdVar),
	numOfPins(BIdVar, NPVar),
	NPVar #> PinInd.

get_objects(_, [], []).
get_objects(AI, [Id0|IdRest], [Obj0|ObjRest]) :-
	get_object(AI, Id0, Obj0),
	get_objects(AI, IdRest, ObjRest).

get_object(AI, ObjId, Obj) :-
	number(ObjId),
	ObjId >= 0,
	nth0(ObjId, AI, Obj).

get_instance_ids(ClassName, Ids, CIds) :-
	classId(ClassName, CId),
	nth0(CId, Ids, CIds).

has_var([Var]) :-
    var(Var).
has_var([First|_]) :-
	var(First), !.
has_var([_|Rest]) :-
    has_var(Rest).
    
no_var([]).
no_var([First|Rest]) :-
    number(First),
    no_var(Rest).

test0 :-
	SubseaField = [[0], [0], [-1], [], [], [0]],
	AI = [SubseaField],
	Ids = [[0], [], [], [], [], []],
	check_system(AI, Ids).

test1([Bind1, Bind2, Bind3, Bind4, M]) :-
        % in the list representing an object after array field an additional item is added which preserves the size of that array field. If this additional item is a variable it means that the array field can be extended by adding to it more items.
	SubseaField = [[0], [0], [-1], [1,3], _, [2]],
	Xt1 = [[1], [1], [0], [2], _, [5], _, [1]],
	SCM1 = [[2], [2], [1], [0], [6, 7], _, [2]],
	Xt2 = [[1], [3], [0], [4], _, [], _, [0]],
	SCM2 = [[2], [4], [3], [0], [8, 9], _, [2]],
	Dev1 = [[4], [5], [1]],
	Sem1 = [[3], [6], [2], [3, 2], [EbV1, EbV2], [2], [10], _, [1]],
	Sem2 = [[3], [7], [2], [1, 1], [EbV3, EbV4], [2], [11], _, [1]],
	Sem3 = [[3], [8], [4], [1,1,1],[EbV5, EbV6, EbV7], [3], [12], _, [1]],
	Sem4 = [[3], [9], [4], [1,1,1],[EbV8, EbV9, EbV10], [3], [13],_, [1]],
	Conn1 = [[5], [10], [6], [13], [PindV1], [Bind1], [Bind1], [5], [DevRefV1]],
	Conn2 = [[5], [11], [7], [PindV2], [PindV2], [Bind2], [Bind2], [5], [DevRefV2]],
	Conn3 = [[5], [12], [8], [PindV3], [PindV3], [Bind3], [Bind3], [DevRefV3], [DevRefV3]],
	Conn4 = [[5], [13], [9], [PindV4], [PindV4], [Bind4], [Bind4], [DevRefV4], [DevRefV4]],
	SubIds = [0],
	XtIds = [1,3],
	SCMIds = [2,4],
	SEMIds = [6, 7, 8, 9],
	DevIds = [5],
	ConnIds = [10, 11, 12, 13],
	AI = [SubseaField, Xt1, SCM1, Xt2, SCM2, Dev1, Sem1, Sem2, Sem3, Sem4, Conn1, Conn2, Conn3, Conn4],
	Ids = [SubIds, XtIds, SCMIds, SEMIds, DevIds, ConnIds],
	check_system(AI, Ids),
	fd_dom(EbV1, EbV1_dom),
	fd_dom(PindV1, PindV1_dom),
	fd_dom(PindV3, PindV3_dom),
	fd_dom(DevRefV3, DevRefV3_dom),
	M = [EbV1_dom, PindV1_dom, PindV3_dom, DevRefV3_dom].


