package main;

import core.IEOS;
import core.tree.Tree;


public class TestData {
	
	private IEOS ieos;
	
	private String EC = "ElectronicConnection";
	private String ebIndex = "ebIndex";
	private String pinIndex = "pinIndex";
	//SEM
	private String SEM = "Sem";
	private String eBoards = "eBoards";
	
	//Enumeration
	private String ElecBoard = "ElecBoard";
	
	
	
	
	
//	Inv1: context ElectronicConnection inv PinRange
//	pinIndex >= 0 and sem.eBoards->asSequence()->at(ebIndex+1).numOfPins > pinIndex
//
//	Inv2: context ElectronicConnection inv BoardIndRange 
//	ebIndex >= 0 and ebIndex < sem.eBoards->size()
	
	private String[] queries = new String[]{
			EC+".allInstances()->forAll(c|c.pinIndex>=0)",
			EC+".allInstances()->forAll(o|o.pinIndex>=0 and " +
										  "o.sem.eBoards->asSequence()->at(o.ebIndex+1).toInteger()>o.pinIndex)",
			
			"ebIndex >= 0 and ebIndex < sem.eBoards->size()"
		};
	
	
	public TestData(){
		ieos = new IEOS();
	}
	
	public IEOS updateObjectDiagram(Setup setup) {
		
		ieos.createClassDiagram();

		//Enumeration ElecBoard
		ieos.insertEnumeration(ElecBoard, new String[]{"64","32","16","8"});
			

		//Class Electronic Connection
		ieos.insertClass(EC);
		ieos.insertAttribute(EC, ebIndex, "Integer");
		ieos.insertAttribute(EC, pinIndex, "Integer");
		//Class SEM
		ieos.insertClass(SEM);
		ieos.insertAttribute(SEM, eBoards, ElecBoard);
		
		//Associations
		ieos.insertAssociation(EC, "ec","1..*","0..1", "sem", SEM);
		
		
		ieos.closeClassDiagram();
		
		/**
		 * Object Diagram
		 */
		
		ieos.createObjectDiagram();

		ieos.insertObject(EC, "ElecCon");
		ieos.insertValue(EC, pinIndex, "ElecCon", "1");
		ieos.insertValue(EC, ebIndex, "ElecCon", "1");
		ieos.insertObject(SEM, "sem");

		ieos.insertLink(EC, "ElecCon", "ec","sem", "sem",SEM);
		
		switch(setup){
		
		case ALLVAR:{
			break;
		}
		}
		
		try {
			ieos.closeObjectDiagram();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Unable to close ObjectDiagram");
		}
		
		return ieos;
		
	
		
	}
	
	public IEOS getIEOS(){
		return ieos;
	}
	
	public String getQuery(int nth_element){
		if(nth_element > queries.length-1){
			System.out.println("Element with that id not present, giving you the first element instead.");
			return queries[0];
		} else {
			return queries[nth_element];
		}
	}
	
	

}
