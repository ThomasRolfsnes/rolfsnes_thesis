package main;

import graphviz.TreeParser;
import core.IEOS;
import core.tree.Tree;

public class Main {
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//Choose which query to run. The list of queries can be found in TestData class
		int chosenQuery = 0;
		//Path to where the graphviz source file should be stored
		String path = "C:\\Users\\Thomas\\Pictures\\out.";


		IEOS ieos;

		TestData data = new TestData();
		
		ieos = data.updateObjectDiagram(Setup.ALLVAR);
		
		ieos.changeToEvaluating();
		
		String query = data.getQuery(chosenQuery);
		System.out.println("Trying query: " + query);
		try {
			System.out.println("Result of evaluation: " + ieos.query(query));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ieos.changeToParsing();
		ieos = data.updateObjectDiagram(Setup.ALLVAR);
		
		Tree tree = null;
		try {
			tree = ieos.parse(query);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Result of parsing: " + tree.print());
		
		System.out.println("Generating dot format..");
		
		TreeParser t = new TreeParser();
		
		System.out.println("\n\nGraphViz code:\n\n"+ t.parse(tree,path));
	}

}
