package simula.experiment;


import graphviz.TreeParser;

import java.io.BufferedWriter;
import java.io.FileWriter;

import core.IEOS;
import core.tree.Node;
import core.tree.Tree;



public class Experiment07 
{

	
	 
	public static void main(String s[]) throws Exception
	{
		String queries[]= new String[9];
		
		queries[1]= "B.allInstances()->forAll(b|b.x=47)"; //P1
		queries[2]= "B.allInstances()->select(b|b.y > 90)->size() > 4 and B.allInstances()->select(b|b.y > 90)->exists(b|b.y=92)"; //p2
		queries[3]= "B.allInstances()->select(b|b.y > 90)->size() > 4 and B.allInstances()->select(b|b.y > 90)->isUnique(b|b.y)"; //p3
		queries[4]= "B.allInstances()->select(b|b.y > 90)->size() > 4 and B.allInstances()->select(b|b.y > 90)->one(b|b.y=95)"; //p4
		queries[5]= "B.allInstances()->collect(b|b.y)->includes(17)"; //not self.isActive implies self.ownedReception.isEmpty() P5
		queries[6]= "B.allInstances()->collect(b|b.y)->excludes(0)"; //P6
		queries[7]= "let c = Set{-1,87,19,88} in B.allInstances()->collect(b|b.y)->includesAll(c)"; //P7
		IEOS ieos;
		String valueToInsert ="";
		String result ="";
		String query=queries[7];
		int[] value = new int[20];
		for (int i=0;i<20;i++)
		{
			valueToInsert=valueToInsert+value[i]+" ";
			
		}
			
		ClassDiagramTestData data = new ClassDiagramTestData();
		try{
			ieos = data.updateObjectDiagram(valueToInsert, query);
			ieos.changeToEvaluating();
			result = ieos.query(query);
			System.out.println("Result: "+result);
			ieos.changeToParsing();
			ieos = data.updateObjectDiagram(valueToInsert, query);
			Tree tree = ieos.parse(query);
			System.out.println("Tree:"+tree.print());
			System.out.println("Root:"+tree.getRootElement());
			TreeParser p = new TreeParser();
			System.out.println(p.parse(tree, null));
//			String xml = toXML(tree.getRootElement());
//			BufferedWriter out = new BufferedWriter(new FileWriter("result.txt"));
//		      out.write(xml);//tree.toXML());
//		      out.close();
			
		}catch (Exception e){
			System.out.println("Error");
			
		}	
	}
	
	static String toXML(Node node) {
	    String result = "";
	    System.out.println(node.getNameOperation());
	    if ((node.getNameOperation().equals("Set")) || (node.getNameOperation().equals("Bag")) || (node.getNameOperation().equals("OrderedSet")) || 
	      (node.getNameOperation().equals("Sequence")) || (node.getNameOperation().equals("Tuple"))) {
	      result = "<" + node.getNameOperation() + ">\n";
	      for (int i = 0; i < node.getChildren().size(); i++) {
	        result = result + toXML(((Node)node.getChildren().get(i)));//.toXML();
	      }
	      result = result + "</" + node.getNameOperation() + ">\n";
	    }
	    else if (node.getNameOperation().equals("tupleAttribute")) {
	      result = "<attr>\n";
	      result = result + "<name>" + node.getInfoOperation()[0] + "</name>\n";
	      result = result + "<value>\n";
	      result = result + toXML(((Node)node.getChildren().get(0)));//.toXML();
	      result = result + "</value>\n";
	      result = result + "</attr>\n";
	    }
	    else if (node.getNameOperation().equals("constant")) {
	      result = "<elem>" + node.getInfoOperation()[0] + "</elem>\n";
	    }
	    System.out.println(result);
	    return result;
	  }
	
	

	public Experiment07() 
	{
	
			
	}
	
	
	
	
	

	
	
	
	
		
	
}




