package simula.experiment;

import core.IEOS; 
import java.util.*;
import core.oclLex.parser.Parser;
import core.tree.Tree;
public class ClassDiagramTestData { 
	private IEOS ieos; 

	public ClassDiagramTestData(){ 
		ieos = new IEOS();
	}

	public IEOS updateObjectDiagram(String updateData, String queryValue) throws Exception{
		
		//System.out.println("ggg"+upDate);
		ieos.createClassDiagram();
		ieos.insertClass("A"); // ieos.insertClass("Class")
		ieos.insertClass("B");
		ieos.insertAssociation("A","","1..1","1..1","b","B");
		ieos.insertAttribute("B", "x", "Integer");
		ieos.insertAttribute("B", "y", "Integer");
		
		ieos.closeClassDiagram();

		ieos.createObjectDiagram();

		
		ieos.insertObject("A","self");
		ieos.insertObject("B","b0");
		ieos.insertObject("B","b1");
		ieos.insertObject("B","b2");
		ieos.insertObject("B","b3");
		ieos.insertObject("B","b4");
		ieos.insertObject("B","b5");
		ieos.insertObject("B","b6");
		ieos.insertObject("B","b7");
		ieos.insertObject("B","b8");
		ieos.insertObject("B","b9");
		
		
		
		ieos.insertLink("A","self","","b","b0","B");
		ieos.insertLink("A","self","","b","b1","B");
		ieos.insertLink("A","self","","b","b2","B");
		
		ieos.insertLink("A","self","","b","b3","B");
		ieos.insertLink("A","self","","b","b4","B");
	
		
		ieos.insertLink("A","self","","b","b5","B");
		ieos.insertLink("A","self","","b","b6","B");
		ieos.insertLink("A","self","","b","b7","B");
		
		ieos.insertLink("A","self","","b","b8","B");
		ieos.insertLink("A","self","","b","b9","B");
		
		
		//ieos.insertLink("A","self","","b","b3","B");
		//ieos.insertLink("A","self","","b","b4","B");
		
		
				
		int i=0;
		StringTokenizer st = new StringTokenizer(updateData);
	     while (st.hasMoreTokens()) {
	    	 String value = st.nextToken();
	    	 
	    
	    		 ieos.insertValue("B","x","b"+i,value);
	    	if (i==9)
	    		break;
	    	
	    	 i++;
	     }
	      i=0;
			
		     while (st.hasMoreTokens()) {
		    	 String value = st.nextToken();
		    	 
		    
		    		 ieos.insertValue("B","y","b"+i,value);
		    	
		    	
		    	 i++;
		     }
	
		
		
		return ieos;
		
		

	}
	
	public IEOS getIEOS()
	{
		return ieos;
	}
	
	

}

