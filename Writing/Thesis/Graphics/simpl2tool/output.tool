<?xml version="1.0" encoding="ISO-8859-1"?>
<xmi:XMI xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:Tool_MM="http:///Tool_MM.ecore">
  <Tool_MM:Enumeration name="SubseaFieldSubtypes">
    <literals>TemplatedSubseaField</literals>
    <literals>ScatteredSubseaField</literals>
  </Tool_MM:Enumeration>
  <Tool_MM:Enumeration name="MCSSubtypes">
    <literals>MCS</literals>
  </Tool_MM:Enumeration>
  <Tool_MM:Enumeration name="SemAppSubtypes">
    <literals>SemApp</literals>
  </Tool_MM:Enumeration>
  <Tool_MM:TopmostConfigurator name="FMCSystem">
    <feature xsi:type="Tool_MM:ConfigurableType" name="subseaFields_type" type="/0"/>
    <feature xsi:type="Tool_MM:ConfigurableType" name="mCSs_type" type="/1"/>
    <feature xsi:type="Tool_MM:ConfigurableType" name="semApps_type" type="/2"/>
    <feature xsi:type="Tool_MM:PartCardinality" name="subseaFields_type_card" type="/22" configurabletype="/3/@feature.0" partType="/0"/>
    <feature xsi:type="Tool_MM:PartCardinality" name="mCSs_type_card" type="/23" partType="/1"/>
    <feature xsi:type="Tool_MM:PartCardinality" name="semApps_type_card" type="/24" partType="/2"/>
  </Tool_MM:TopmostConfigurator>
  <Tool_MM:Configurator name="DeviceController">
    <feature xsi:type="Tool_MM:ConfigurableAttribute" name="fullName" type="String"/>
    <feature xsi:type="Tool_MM:ConfigurableAttribute" name="location" type="String"/>
    <feature xsi:type="Tool_MM:ConfigurableAttribute" name="heartBeatTime" type="Integer"/>
    <feature xsi:type="Tool_MM:AttributeCardinality" name="heartBeatTime_cardinality" type="/25" configurableattribute="/4/@feature.2"/>
  </Tool_MM:Configurator>
  <Tool_MM:Enumeration name="ControlModuleSubtypes">
    <literals>SCM</literals>
  </Tool_MM:Enumeration>
  <Tool_MM:Configurator name="XmasTree" super="/9">
    <feature xsi:type="Tool_MM:ConfigurableType" name="SCM_type" type="/5"/>
  </Tool_MM:Configurator>
  <Tool_MM:Enumeration name="ManifoldSubtypes">
    <literals>Manifold</literals>
  </Tool_MM:Enumeration>
  <Tool_MM:Enumeration name="XmasTreeSubtypes">
    <literals>XmasTree</literals>
  </Tool_MM:Enumeration>
  <Tool_MM:Configurator name="ScatteredSubseaField">
    <feature xsi:type="Tool_MM:ConfigurableType" name="manifold_type" type="/7"/>
    <feature xsi:type="Tool_MM:ConfigurableType" name="xmasTree_type" type="/8"/>
    <feature xsi:type="Tool_MM:PartCardinality" name="manifold_type_card" type="/26" partType="/7"/>
    <feature xsi:type="Tool_MM:PartCardinality" name="xmasTree_type_card" type="/27" partType="/8"/>
  </Tool_MM:Configurator>
  <Tool_MM:Enumeration name="ControlModuleSubtypes">
    <literals>SCM</literals>
  </Tool_MM:Enumeration>
  <Tool_MM:Configurator name="Manifold">
    <feature xsi:type="Tool_MM:ConfigurableType" name="MCM_type" type="/10"/>
  </Tool_MM:Configurator>
  <Tool_MM:Enumeration name="TemplateSubtypes">
    <literals>Template</literals>
  </Tool_MM:Enumeration>
  <Tool_MM:Configurator name="TemplatedSubseaField">
    <feature xsi:type="Tool_MM:ConfigurableType" name="templates_type" type="/12"/>
    <feature xsi:type="Tool_MM:PartCardinality" name="templates_type_card" type="/28" partType="/12"/>
  </Tool_MM:Configurator>
  <Tool_MM:Enumeration name="ManifoldSubtypes">
    <literals>Manifold</literals>
  </Tool_MM:Enumeration>
  <Tool_MM:Configurator name="Template">
    <feature xsi:type="Tool_MM:ConfigurableType" name="manifold_type" type="/14"/>
  </Tool_MM:Configurator>
  <Tool_MM:Enumeration name="DeviceControllerSubtypes">
    <literals>SensorController</literals>
    <literals>ChokeController</literals>
    <literals>ValveController</literals>
  </Tool_MM:Enumeration>
  <Tool_MM:Configurator name="SemApp">
    <feature xsi:type="Tool_MM:ConfigurableType" name="controllers_type" type="/16"/>
    <feature xsi:type="Tool_MM:PartCardinality" name="controllers_type_card" type="/29" configurabletype="/17/@feature.0" partType="/16"/>
  </Tool_MM:Configurator>
  <Tool_MM:UserDefinedType name="Choke"/>
  <Tool_MM:UserDefinedType name="Sensor"/>
  <Tool_MM:UserDefinedType name="Valve"/>
  <Tool_MM:Configurator name="SEM">
    <feature xsi:type="Tool_MM:ConfigurableTopology" name="chokes" type="/18"/>
    <feature xsi:type="Tool_MM:ConfigurableTopology" name="sensors" type="/19"/>
    <feature xsi:type="Tool_MM:ConfigurableTopology" name="valves" type="/20"/>
    <feature xsi:type="Tool_MM:TopologyCardinality" name="chokes_cardinality" type="/30" configurabletopology="/21/@feature.0"/>
    <feature xsi:type="Tool_MM:TopologyCardinality" name="sensors_cardinality" type="/31" configurabletopology="/21/@feature.1"/>
    <feature xsi:type="Tool_MM:TopologyCardinality" name="valves_cardinality" type="/32" configurabletopology="/21/@feature.2"/>
  </Tool_MM:Configurator>
  <Tool_MM:IntegerInterval upper="-1" lower="1"/>
  <Tool_MM:IntegerInterval upper="-1" lower="1"/>
  <Tool_MM:IntegerInterval upper="-1"/>
  <Tool_MM:IntegerInterval upper="5" lower="1"/>
  <Tool_MM:IntegerInterval upper="1"/>
  <Tool_MM:IntegerInterval upper="-1" lower="1"/>
  <Tool_MM:IntegerInterval upper="-1" lower="1"/>
  <Tool_MM:IntegerInterval upper="-1" lower="1"/>
  <Tool_MM:IntegerInterval upper="-1"/>
  <Tool_MM:IntegerInterval upper="-1"/>
  <Tool_MM:IntegerInterval upper="-1"/>
</xmi:XMI>
